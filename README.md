# PowerDNS Admin

## Runtime Requirements
 * Java SDK version 6 or later
 * Java application server such as Tomcat, Jetty, Glassfish, etc.

## Installation
 * Setup database and credentials
 * Place WAR file in java application server webapps dir

## Development Requirements ##

* [Java SDK](http://java.com/en/download/)
* [Grails](http://grails.org/download)

### OSX Installation
 * Use homebrew to install Grails: ```brew install grails``` or download manually.
 * Set the shell environment variable *GRAILS_HOME* to eg. */usr/local/opt/grails/libexec*.
 * If you use IntelliJ Idea, Netbeans or any other IDE that supports Groovy and Grails, you might not need to set the *GRAILS_HOME* env.


### PowerDNS Setup

To use the suspend button, you must alter the pdns sql backend query, to test for 'ACTIVE = 1' or similar.



## TODO

### DNSSEC

    create table domainmetadata (
     id         SERIAL PRIMARY KEY,
     domain_id  INT REFERENCES domains(id) ON DELETE CASCADE,
     kind       VARCHAR(16),
     content    TEXT
    );
    create index domainidmetaindex on domainmetadata(domain_id);


    create table cryptokeys (
     id         SERIAL PRIMARY KEY,
     domain_id  INT REFERENCES domains(id) ON DELETE CASCADE,
     flags      INT NOT NULL,
     active     BOOL,
     content    TEXT
    );
    create index domainidindex on cryptokeys(domain_id);


    create table tsigkeys (
     id         SERIAL PRIMARY KEY,
     name       VARCHAR(255),
     algorithm  VARCHAR(50),
     secret     VARCHAR(255),
     constraint c_lowercase_name check (((name)::text = lower((name)::text)))
    );

    create unique index namealgoindex on tsigkeys(name, algorithm);
