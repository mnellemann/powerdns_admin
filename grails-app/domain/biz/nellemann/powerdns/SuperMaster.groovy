/**
 *   Copyright (C) 2014  Mark Nellemann <mark.nellemann@gmail.com>
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package biz.nellemann.powerdns

/*
    This is slave auto provisioning
    We add known master nameservers, and if they notify us, pdns will add the slave zone

    Will this interfere with PowerDNS Admin ??


    To configure a supermaster with IP address 10.0.0.11 which lists this installation as 'autoslave.powerdns.com', issue the following:

	insert into supermasters ('10.0.0.11','autoslave.powerdns.com','internal');

    From now on, valid notifies from 10.0.0.11 that list a NS record containing 'autoslave.powerdns.com'
    will lead to the provisioning of a slave domain under the account 'internal'.
 */

class SuperMaster {

    def validationService

    String ip               // The IP address of the
    String nameserver       // The authorative nameserver for the domain
    String account = 'internal'

    static constraints = {
        ip(blank: false, unique: true, size: 1..46, validator: { val, obj -> return obj.validationService.validateIpAddress(val)})
        nameserver(blank: false, validator: { val, obj -> return obj.validationService.validateDomainName(val, false) }  )
        account(blank: true, nullable: true, size: 1..128 )
    }

    // GORM Database Mappings
    static mapping = {
        table 'supermasters'
        id generator: 'identity'
        version false
        autoTimestamp false
    }
}
