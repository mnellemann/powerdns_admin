/**
 *   Copyright (C) 2014  Mark Nellemann <mark.nellemann@gmail.com>
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package biz.nellemann.powerdns

class Record {

    // See PowerDNS types for more information: http://doc.powerdns.com/html/types.html

    // Use our own validation service for custom validation
    def validationService

    // Domain Relationships
    static belongsTo = [ domain: Domain ]

    // Domain Properties
    String name                 // What URI the dns-server should pick up on. For example www.test.com
    String type = null          // Record Types: SOA, NS, MX, A, AAAA, CNAME, TXT, PTR, HWINFO, SRV, NAPTR
    String content = null       // Is the answer of the DNS-query and the content depend on the type field.
    Integer ttl = 8600          // How long the DNS-client are allowed to remember this record. Also known as Time To Live(TTL) This value is in seconds.
    Integer prio = 0            // This field sets the priority of an MX-field.
    Integer change_date         // Timestamp for the last update
    String ordername
    Boolean auth

    // We don't want to persists our types into the database
    static transients = [ 'types' ]
    List<String> types = [ 'A', 'CNAME', 'MX', 'NS', 'TXT', 'SRV' ]


    // Domain Validation Constraints
    static constraints = {
        name(blank: false, size: 1..254, validator: { val, obj -> return obj.validationService.validateRecordName(val, obj.type, obj.domain.name, obj.domain.reverse) })
        type(blank:false, size: 1..10, validator: { val, obj -> return obj.validationService.validateRecordType(val, obj.types, obj.domain.reverse) })
        content(blank: false, size: 1..254, validator: { val, obj -> return obj.validationService.validateRecordContent(val, obj.type, obj.name, obj.domain.name, obj.domain.reverse) })                                       // TODO: Validation depends on record type
        ttl(blank: false, min: 300, max: 86400*5)                           // TODO: Use default server / user preferences
        prio(blank: false, default:0, min:0, max: 99)                       // TODO: Validation depends on record type (eg. MX)
        change_date(blank: true, nullable: true, min: 0, max: 2147468400)   // TODO: Auto timestamp of now()?
        ordername(blank: true, nullable: true, size: 1..254)
        auth(boolean: true, default: false, nullable: true)
    }


    // GORM Database Mappings
    static mapping = {
        table 'records'
        version false
        autoTimestamp false
        id generator: 'identity'
        sort type: "asc"
        name index: 'idx_record_name'
        type index: 'idx_record_type'
        ordername index: 'idx_record_ordername'
    }


    // Override: return name of zone
    String toString() {
        return "${name} ${type} ${content} ${ttl} ${prio}"
    }

    List<String> getTypes() {
        def zone = Zone.get(this.domain.id)
        if(zone.reverse) {
            return ['PTR', 'NS']
        } else {
            return this.types
        }
    }

    // Hook that let's us update the change_date field
    def beforeUpdate() {

        // Set change_date to current time
        this.change_date = System.currentTimeMillis()/1000
        println("Updating change_date for record: ${this.toString()}")

    }
}
