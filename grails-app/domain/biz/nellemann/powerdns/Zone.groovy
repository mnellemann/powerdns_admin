/**
 *   Copyright (C) 2014  Mark Nellemann <mark.nellemann@gmail.com>
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package biz.nellemann.powerdns

class Zone extends Domain {

    def validationService

    // Domain Relationships
    static belongsTo = [ user: User ]

    String soa_primary              // eg. ns.comin.dk
    String soa_hostmaster           // eg. hostmaster@comin.dk
    Integer soa_serial = 0          // 0 (the default) the highest change_date field of all records within the zone will be used
    Integer soa_refresh = 10800     // 3 hours
    Integer soa_retry = 3600        // 1 hour
    Integer soa_expire = 604800     // 1 week
    Integer soa_ttl = 3600          // 1 hour
    Boolean active = true           // Enabled for editing ? suspended ..?
    Boolean reverse = false         // Reverse zone?
    Date dateCreated
    Date lastUpdated


    static constraints = {
        //name(blank: false, unique: true, size: 1..254, validator: { val, obj -> return obj.validationService.validateDomainName(val, obj.reverse) })
        soa_primary(blank: false, matches: '^[A-Za-z0-9-]+(\\.[A-Za-z0-9-]+)*(\\.[A-Za-z]{2,4})$')
        soa_hostmaster(blank: false, email: true)
        soa_serial(min: 0, max: 2147483647)  // Real max should be 4294967295
        soa_refresh(min: 0, max: 2147483647)
        soa_retry(min: 0, max: 2147483647)
        soa_expire(min: 0, max: 2147483647)
        soa_ttl(min:0, max:2147483647)
        active(boolean: true, default: true)
        reverse(boolean: true, default: false)
    }


    // GORM Database Mappings
    static mapping = {
        //tablePerHierarchy false
        version true
        autoTimestamp true
        sort lastUpdated: "desc"
        //name index: 'idx_zone_name'
        //records cascade:"all-delete-orphan"     // Delete all records, if domain is deleted
    }


    // Override: return name of zone
    String toString() {
        return "${name}"
    }

    // Hook that let's us insert records on create
    def afterInsert() {

        // New domain without SOA record, add SOA and NS for primary
        this.addToRecords(new Record(name: "${this.name}", type: 'SOA', content: "${this.soa_primary} ${this.soa_hostmaster} ${this.soa_serial} ${this.soa_refresh} ${this.soa_retry} ${this.soa_expire} ${this.soa_ttl}"))
        this.addToRecords(new Record(name: "${this.name}", type: 'NS', content: "${this.soa_primary}"))

        // Add other standard records to domain - in future from template ?
        if(! this.reverse) {
            this.addToRecords(new Record(name: "${this.name}", type: 'A', content: "127.0.0.1"))
            this.addToRecords(new Record(name: "www.${this.name}", type: 'CNAME', content: "${this.name}"))

            this.addToRecords(new Record(name: "mail.${this.name}", type: 'A', content: "127.0.0.1"))
            this.addToRecords(new Record(name: "${this.name}", type: 'MX', prio: 10, content: "mail.${this.name}"))
        }
    }


    // Hook that let's us update the SOA record
    def afterUpdate() {

        // Insert or Update SOA record for domain
        def r = Record.findByDomainAndType(this, 'SOA')
        if(r != null) {
            r.content = "${this.soa_primary} ${this.soa_hostmaster} ${this.soa_serial} ${this.soa_refresh} ${this.soa_retry} ${this.soa_expire} ${this.soa_ttl}"
            r.save()
        }

    }

}
