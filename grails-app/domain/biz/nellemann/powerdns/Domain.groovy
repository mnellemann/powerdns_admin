/**
 *   Copyright (C) 2014  Mark Nellemann <mark.nellemann@gmail.com>
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package biz.nellemann.powerdns

class Domain {

    def validationService

    // Domain Relationships
    static hasMany = [ records: Record ]

    // Standard PDNS Properties
    String name                     // This field is the actual domainname. This is the field that powerDNS matches to when it gets a request. The domainname should be in the format of: domainname.TLD
    String master = null            // This describes the master nameserver from which this domain should be slaved. Takes IPs, not hostnames! Only for traditional SLAVE zones, not NATIVE
    Integer last_check = null       // This value, updated by PowerDNS, is the unix timestamp of the last successful attempt to check this zone for freshness on the master
    String type = "NATIVE"          // Describes how PowerDNS should host the zone. Valid values are NATIVE, MASTER, and SLAVE.
    Integer notified_serial = null  // The last notified serial of a master domain. This is updated from the SOA record of the domain.
    String account = null           // Determine if a certain host is a supermaster for a certain domain name.


    // Domain Validation Constraints
    static constraints = {
        name(blank: false, unique: true, size: 1..254, validator: { val, obj -> return obj.validationService.validateDomainName(val, obj.reverse) })
        master(blank: true, nullable: true, size: 1..128)        // TODO: if type == slave, then: validator: { return InetAddressValidator.getInstance().isValidInet4Address(it) }
        last_check(min: 0, max: 2147468400, blank: true, nullable: true)
        type(blank: false, inList: ['NATIVE', 'MASTER', 'SLAVE'])
        notified_serial(min: 0, max: 2147468400, blank: true, nullable: true)
        account(display: false, blank: true, nullable: true, size: 1..40)
    }


    // GORM Database Mappings
    static mapping = {
        tablePerHierarchy false
        table 'domains'
        version false
        autoTimestamp false
        id generator: 'identity'
        sort name: "asc"
        name index: 'idx_domain_name'
    }


    // Override: return name of zone
    String toString() {
        return "${name}"
    }

}
