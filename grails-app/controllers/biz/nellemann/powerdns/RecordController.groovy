/**
 *   Copyright (C) 2014  Mark Nellemann <mark.nellemann@gmail.com>
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package biz.nellemann.powerdns

import static org.springframework.http.HttpStatus.*
import grails.transaction.Transactional
import grails.plugin.springsecurity.annotation.Secured

@Transactional(readOnly = true)
@Secured(['ROLE_ADMIN', 'ROLE_USER'])
class RecordController {

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    def springSecurityService

    def index(Integer max) {

        params.max = Math.min(max ?: 10, 500)

        // We make sure the domain exists and is owned by the logged-in user
        def d = Zone.findByIdAndUser(params.domain?.id, springSecurityService.currentUser)
        if(d == null) {
            println("No such domain / available to user:  ${params.toString()}")
            redirect(controller: 'zone')
        }

        // Get list of records
        def result = Record.createCriteria().list(params) {
            eq('domain', d)
            and {
                ne('type', 'SOA')
            }
        }
        respond(result, model: [ recordInstanceCount: Record.findAllByDomain(d).size(), domainInstance: d ])

    }

    def show(Record recordInstance) {
        // TODO: restrict to logged-in user
        respond recordInstance
    }

    def create() {
        respond new Record(params)
    }

    @Transactional
    def save(Record recordInstance) {

        if (recordInstance == null) {
            notFound()
            return
        }

        // We make sure the domain exists and is owned by the logged-in user
        def d = Zone.findByIdAndUser(params.domain?.id, springSecurityService.currentUser)
        if(d == null) {
            println("No such domain / available to user:  ${params.toString()}")
            redirect(controller: 'zone')
        }

        // Set TTL if empty
        // TODO: lookup user or global preferences for default ttl
        if(!recordInstance.ttl) {
            recordInstance.ttl = 8600
        }

        if (recordInstance.hasErrors()) {
            respond recordInstance.errors, view:'create'
            return
        }

        recordInstance.save flush:true


        request.withFormat {
            form {
                flash.message = message(code: 'default.created.message', args: [message(code: 'recordInstance.label', default: 'Record'), recordInstance.id])
                redirect(action: 'index', method: 'GET', params: [ 'domain.id': "${d.id}" ])
            }
            '*' { respond recordInstance, [status: CREATED] }
        }
    }

    def edit(Record recordInstance) {

        // We make sure the domain exists and is owned by the logged-in user
        def d = Zone.findByIdAndUser(recordInstance.domain?.id, springSecurityService.currentUser)
        if(d == null) {
            println("No such domain / available to user:  ${params.toString()}")
            redirect(controller: 'zone')
        }

        respond recordInstance
    }

    @Transactional
    def update(Record recordInstance) {

        if (recordInstance == null) {
            notFound()
            return
        }

        // We make sure the domain exists and is owned by the logged-in user
        def d = Zone.findByIdAndUser(params.domain?.id, springSecurityService.currentUser)
        if(d == null) {
            println("No such domain / available to user:  ${params.toString()}")
            redirect(controller: 'zone')
        }

        if (recordInstance.hasErrors()) {
            respond recordInstance.errors, view:'/record/edit'
            return
        }

        recordInstance.save flush:

        request.withFormat {
            form {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'Record.label', default: 'Record'), recordInstance.id])
                redirect(action: 'index', method: 'GET', params: [ 'domain.id': "${d.id}" ])
            }
            '*'{ respond recordInstance, [status: OK] }
        }
    }


    @Transactional
    def deleteItem(Record recordInstance) {

        // We make sure the domain exists and is owned by the logged-in user
        def d = Zone.findByIdAndUser(recordInstance.domain?.id, springSecurityService.currentUser)
        if(d == null) {
            println("No such domain / available to user:  ${params.toString()}")
            redirect(controller: 'zone')
        }

        // Delete the record
        recordInstance.delete flush:true

        // Get new list of records
        def result = Record.createCriteria().list(params) {
            eq('domain', d)
            and {
                ne('type', 'SOA')
            }
        }
        render(template: 'list', model: [recordInstanceList: result])
    }


    @Transactional
    def delete(Record recordInstance) {
        if (recordInstance == null) {
            notFound()
            return
        }

        println("Record instance ID: ${recordInstance.id}")


        // We make sure the domain exists and is owned by the logged-in user
        def d = Zone.findByIdAndUser(recordInstance.domain?.id, springSecurityService.currentUser)
        if(d == null) {
            println("No such domain / available to user:  ${params.toString()}")
            redirect(controller: 'zone')
        }


        recordInstance.delete flush:true

        request.withFormat {
            form {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'Record.label', default: 'Record'), recordInstance.id])
                redirect(action: 'index', method: 'GET', params: [ 'domain.id': "${d.id}" ])
            }
            '*'{ render status: NO_CONTENT }
        }
    }

    protected void notFound() {
        request.withFormat {
            form {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'recordInstance.label', default: 'Record'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }

}
