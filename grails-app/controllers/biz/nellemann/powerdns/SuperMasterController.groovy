/**
 *   Copyright (C) 2014  Mark Nellemann <mark.nellemann@gmail.com>
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package biz.nellemann.powerdns

import grails.transaction.Transactional
import grails.plugin.springsecurity.annotation.Secured

@Transactional(readOnly = true)
@Secured(['ROLE_ADMIN'])
class SuperMasterController {

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    def index(Integer max) {
        params.max = Math.min(max ?: 10, 100)

        def result = SuperMaster.list()
        respond(result, model:[ superMasterInstanceCount: result.size() ])
    }


    def show(SuperMaster superMasterInstance) {
        respond superMasterInstance
    }

    def create() {
        respond new SuperMaster(params)
    }


    @Transactional
    def save(SuperMaster superMasterInstance) {
        if (superMasterInstance == null) {
            notFound()
            return
        }

        // Validate superMaster
        if (superMasterInstance.hasErrors()) {
            respond superMasterInstance.errors, view:'create'
            return
        }

        // Save the instance
        superMasterInstance.save flush:true

        request.withFormat {
            form {
                flash.message = message(code: 'default.created.message', args: [message(code: 'superMasterInstance.label', default: 'SuperMaster'), superMasterInstance.id])
                redirect action: "index", method: "GET"
            }
            '*' { respond superMasterInstance, [status: CREATED] }
        }
    }
           
    
    def edit(SuperMaster superMasterInstance) {

        // Lookup domain provided in request
        if (superMasterInstance == null) {
            notFound()
            return
        }

        respond superMasterInstance
    }

    @Transactional
    def update(SuperMaster superMasterInstance) {

        if (superMasterInstance == null) {
            notFound()
            return
        }

        // Validate domain
        if (superMasterInstance.hasErrors()) {
            respond superMasterInstance.errors, view:'edit'
            return
        }

        superMasterInstance.save flush:true

        request.withFormat {
            form {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'SuperMaster.label', default: 'SuperMaster'), superMasterInstance.id])
                redirect action: "index", method: "GET" //superMasterInstance
            }
            '*'{ respond superMasterInstance, [status: OK] }
        }
    }

    @Transactional
    def delete(SuperMaster superMasterInstance) {

        if (superMasterInstance == null) {
            notFound()
            return
        }

        // Delete it
        superMasterInstance.delete(flush:true)

        request.withFormat {
            form {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'supermaster.label', default: 'SuperMaster'), superMasterInstance.id])
                redirect action:"index", method:"GET"
            }
            '*'{ render status: NO_CONTENT }
        }
    }

    protected void notFound() {
        request.withFormat {
            form {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'supermaster.label', default: 'SuperMaster'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }

}
