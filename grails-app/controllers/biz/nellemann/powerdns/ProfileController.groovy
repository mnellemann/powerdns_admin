package biz.nellemann.powerdns

import biz.nellemann.powerdns.User
import grails.transaction.Transactional
import static org.springframework.http.HttpStatus.OK

class ProfileController {


    def springSecurityService
    def passwordEncoder


    @Transactional
    def update(User userInstance) {

        println("in user update")

        if (userInstance == null) {
            notFound()
            return
        }

        // We make sure the user is in-fact the logged-in user
        if(userInstance.id != springSecurityService.principal.id) {
            println("No such user / user not logged in:  ${params.toString()}")
            redirect(uri: '/')
        }

        // Validate domain
        if (userInstance.hasErrors()) {
            respond userInstance.errors, view:'/profile/edit'
            return
        }

        userInstance.save flush:true

        request.withFormat {
            form {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'User.label', default: 'User'), userInstance.id])
                redirect action: "edit", method: "GET" //zoneInstance
            }
            '*'{ respond userInstance, [status: OK] }
        }
    }


    @Transactional
    def updatePassword() {

        def userInstance = User.get(springSecurityService.principal.id)
        if (userInstance == null) {
            flash.message = 'Sorry, an error has occurred'
            redirect controller: 'login', action: 'auth'
            println("error 1")
            return
        }

        String password = params.password
        String newPassword = params.password_new
        String newPassword2 = params.password_new_2
        if (!password || !newPassword || !newPassword2 || newPassword != newPassword2) {
            flash.error = 'Please enter your current password and a valid new password'
            render view: 'password', model: [username: session['SPRING_SECURITY_LAST_USERNAME']]
            println("error 2")
            return
        }

        if (!passwordEncoder.isPasswordValid(userInstance.password, password, null /*salt*/)) {
            flash.error = 'Current password is incorrect'
            render view: 'password', model: [username: session['SPRING_SECURITY_LAST_USERNAME']]
            println("error 3")
            return
        }

        if (passwordEncoder.isPasswordValid(userInstance.password, newPassword, null /*salt*/)) {
            flash.error = 'Please choose a different password from your current one'
            render view: 'password', model: [username: session['SPRING_SECURITY_LAST_USERNAME']]
            println("error 4")
            return
        }

        userInstance.password = newPassword
        userInstance.passwordExpired = false

        // Validate domain
        if (userInstance.hasErrors()) {
            respond userInstance.errors, view:'/profile/edit'
            return
        }

        userInstance.save flush:true

        redirect controller: 'logout'
    }


    def password() {

        def userInstance = User.get(springSecurityService.principal.id)

        println("in user password")

        // Lookup user provided in request
        if (userInstance == null) {
            notFound()
            return
        }

        respond userInstance
    }


    def edit() {

        def userInstance = User.get(springSecurityService.principal.id)


        println("in user edit")

        // Lookup user provided in request
        if (userInstance == null) {
            notFound()
            return
        }

        respond userInstance
    }



    protected void notFound() {
        request.withFormat {
            form {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'zoneInstance.label', default: 'Zone'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }

}
