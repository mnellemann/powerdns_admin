package biz.nellemann.powerdns

import grails.plugin.springsecurity.annotation.Secured
import grails.transaction.Transactional

@Secured(['ROLE_ADMIN', 'ROLE_USER'])
class TemplateController {

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    def springSecurityService

    def index(Integer max) {
        params.max = Math.min(max ?: 10, 100)

        // Get list of domains belonging to logged-in user
        def user = User.get(springSecurityService.principal.id)
        def result = Template.createCriteria().list(params) {
            eq('user', user)
        }

        respond(result, model:[ templateInstanceCount: Template.findAllByUser(user).size() ])
    }

    def show(Template templateInstance) {

        // We make sure the domain exists and is owned by the logged-in user
        def d = Template.findByIdAndUser(templateInstance?.id, springSecurityService.currentUser)
        if(d == null) {
            notFound()
            return
        }

        respond templateInstance
    }

    def create() {
        respond new Template(params)
    }

    @Transactional
    def save(Template templateInstance) {
        if (templateInstance == null) {
            notFound()
            return
        }

        // Validate domains
        if (templateInstance.hasErrors()) {
            respond templateInstance.errors, view:'create'
            return
        }

        // Add domain to logged-in user
        def user = User.find(springSecurityService.currentUser)
        user.addToTemplates(templateInstance)

        // Save the domain
        templateInstance.save flush:true

        request.withFormat {
            form {
                flash.message = message(code: 'default.created.message', args: [message(code: 'templateInstance.label', default: 'Template'), templateInstance.id])
                redirect action: "index", method: "GET" //templateInstance
            }
            '*' { respond templateInstance, [status: CREATED] }
        }
    }

    def edit(Template templateInstance) {

        // Lookup domain provided in request
        if (templateInstance == null) {
            notFound()
            return
        }

        // We make sure the domain exists and is owned by the logged-in user
        def d = Template.findByIdAndUser(templateInstance?.id, springSecurityService.currentUser)
        if(d == null) {
            notFound()
            return
        }

        respond templateInstance
    }

    @Transactional
    def update(Template templateInstance) {

        if (templateInstance == null) {
            notFound()
            return
        }

        // We make sure the domain exists and is owned by the logged-in user
        def d = Template.findByIdAndUser(templateInstance?.id, springSecurityService.currentUser)
        if(d == null) {
            notFound()
            return
        }

        // Validate domain
        if (templateInstance.hasErrors()) {
            respond templateInstance.errors, view:'edit'
            return
        }

        templateInstance.save flush:true

        request.withFormat {
            form {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'Template.label', default: 'Template'), templateInstance.id])
                redirect action: "index", method: "GET" //templateInstance
            }
            '*'{ respond templateInstance, [status: OK] }
        }
    }

    @Transactional
    def delete(Template templateInstance) {

        if (templateInstance == null) {
            notFound()
            return
        }

        // We make sure the domain exists and is owned by the logged-in user
        def d = Template.findByIdAndUser(templateInstance?.id, springSecurityService.currentUser)
        if(d == null) {
            notFound()
            return
        }

        // Delete the domain
        templateInstance.delete(flush:true)

        request.withFormat {
            form {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'Template.label', default: 'Template'), templateInstance.id])
                redirect action:"index", method:"GET"
            }
            '*'{ render status: NO_CONTENT }
        }
    }

    protected void notFound() {
        request.withFormat {
            form {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'templateInstance.label', default: 'Template'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }
}
