/**
 *   Copyright (C) 2014  Mark Nellemann <mark.nellemann@gmail.com>
 *
 *   This program is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package biz.nellemann.powerdns

import grails.transaction.Transactional
import grails.plugin.springsecurity.annotation.Secured

@Transactional(readOnly = true)
@Secured(['ROLE_ADMIN', 'ROLE_USER'])
class ZoneController {

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    def springSecurityService

    def index(Integer max) {
        params.max = Math.min(max ?: 10, 100)

        // Get list of domains belonging to logged-in user
        def user = User.get(springSecurityService.principal.id)
        def result = Zone.createCriteria().list(params) {
            eq('user', user)
        }

        respond(result, model:[ zoneInstanceCount: Zone.findAllByUser(user).size() ])
    }


    def suspendItem(Zone zoneInstance) {

        // We make sure the domain exists and is owned by the logged-in user
        def d = Zone.findByIdAndUser(zoneInstance?.id, springSecurityService.currentUser)
        if(d == null) {
            notFound()
            return
        }

        zoneInstance.active = false
        zoneInstance.save(flush:true)

        def result = Zone.findAllByUser(springSecurityService.currentUser)
        render(template: 'list', model: [zoneInstanceCount: result.size(), zoneInstanceList: result])
    }

    def releaseItem(Zone zoneInstance) {

        // We make sure the domain exists and is owned by the logged-in user
        def d = Zone.findByIdAndUser(zoneInstance?.id, springSecurityService.currentUser)
        if(d == null) {
            notFound()
            return
        }

        zoneInstance.active = true
        zoneInstance.save(flush:true)

        def result = Zone.findAllByUser(springSecurityService.currentUser)
        render(template: 'list', model: [zoneInstanceCount: result.size(), zoneInstanceList: result])
    }

    def show(Zone zoneInstance) {

        // We make sure the domain exists and is owned by the logged-in user
        def d = Zone.findByIdAndUser(zoneInstance?.id, springSecurityService.currentUser)
        if(d == null) {
            notFound()
            return
        }

        respond zoneInstance
    }

    def create() {
        respond new Zone(params)
    }

    @Transactional
    def save(Zone zoneInstance) {
        if (zoneInstance == null) {
            notFound()
            return
        }

        // Validate domains
        if (zoneInstance.hasErrors()) {
            respond zoneInstance.errors, view:'create'
            return
        }

        // Add domain to logged-in user
        def user = User.find(springSecurityService.currentUser)
        user.addToZones(zoneInstance)

        // Save the domain
        zoneInstance.save flush:true

        request.withFormat {
            form {
                flash.message = message(code: 'default.created.message', args: [message(code: 'zoneInstance.label', default: 'Zone'), zoneInstance.id])
                redirect action: "index", method: "GET" //zoneInstance
            }
            '*' { respond zoneInstance, [status: CREATED] }
        }
    }

    def edit(Zone zoneInstance) {

        // Lookup domain provided in request
        if (zoneInstance == null) {
            notFound()
            return
        }

        // We make sure the domain exists and is owned by the logged-in user
        def d = Zone.findByIdAndUser(zoneInstance?.id, springSecurityService.currentUser)
        if(d == null) {
            notFound()
            return
        }

        respond zoneInstance
    }

    @Transactional
    def update(Zone zoneInstance) {

        if (zoneInstance == null) {
            notFound()
            return
        }

        // We make sure the domain exists and is owned by the logged-in user
        def d = Zone.findByIdAndUser(zoneInstance?.id, springSecurityService.currentUser)
        if(d == null) {
            notFound()
            return
        }

        // Validate domain
        if (zoneInstance.hasErrors()) {
            respond zoneInstance.errors, view:'edit'
            return
        }

        zoneInstance.save flush:true

        request.withFormat {
            form {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'Zone.label', default: 'Zone'), zoneInstance.id])
                redirect action: "index", method: "GET" //zoneInstance
            }
            '*'{ respond zoneInstance, [status: OK] }
        }
    }

    @Transactional
    def delete(Zone zoneInstance) {

        if (zoneInstance == null) {
            notFound()
            return
        }

        // We make sure the domain exists and is owned by the logged-in user
        def d = Zone.findByIdAndUser(zoneInstance?.id, springSecurityService.currentUser)
        if(d == null) {
            notFound()
            return
        }

        // Delete the domain
        zoneInstance.delete(flush:true)

        request.withFormat {
            form {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'Zone.label', default: 'Zone'), zoneInstance.id])
                redirect action:"index", method:"GET"
            }
            '*'{ render status: NO_CONTENT }
        }
    }

    protected void notFound() {
        request.withFormat {
            form {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'zoneInstance.label', default: 'Zone'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }

}
