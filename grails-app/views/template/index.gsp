<%@ page import="biz.nellemann.powerdns.Template" %>
<!DOCTYPE html>
<html>
<head>
    <g:set var="entityName" value="${message(code: 'template.label', default: 'Template')}"/>
    <title><g:message code="default.list.label" args="[entityName]"/></title>
    <meta name="layout" content="main">
    <r:require modules="bootstrap"/>
</head>

<body>

<!-- Breadcrumbs -->
<div class="row">
    <ol class="breadcrumb">
        <li class="active">Templates</li>
    </ol>
</div>

<!-- Alerts and header -->
<div class="row">
    <g:if test="${flash.message}">
        <div class="alert alert-info" role="status">${flash.message}</div>
    </g:if>
</div>

<!-- Create new zone -->
<div class="row">
    <div class="text-info">
        <p><span class="glyphicon glyphicon-plus-sign"></span> <g:link action="create" params="['user.id': sec.loggedInUserInfo(field: 'id').toInteger()]">Create a new Template</g:link></p>
    </div>
</div>


<!-- List of zones -->
<div class="row">
    <div class="table-responsive" id="listItems">
        <g:render template="list" />
    </div>
    <div>
        <g:paginate total="${templateInstanceCount ?: 0}" class="pagination-sm" />
    </div>
</div>

</body>
</html>
