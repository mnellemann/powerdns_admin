<!DOCTYPE html>
<html>
	<head>
		<title><g:if env="development">Grails Runtime Exception</g:if><g:else>Error</g:else></title>
		<meta name="layout" content="main">
		<g:if env="development"><link rel="stylesheet" href="${resource(dir: 'css', file: 'errors.css')}" type="text/css"></g:if>
	</head>
	<body>

        <div class="row">
            <g:if env="development">
                <div class="pre-scrollable"></div>
                    <g:renderException exception="${exception}" />
                </div>
            </g:if>
            <g:else>
                <div class="well-sm alert">
                    <ul class="errors">
                        <li>An error has occurred</li>
                    </ul>
                </div>
            </g:else>
        </div>

	</body>
</html>
