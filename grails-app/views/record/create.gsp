<!DOCTYPE html>
<html>
<head>
    <g:set var="entityName" value="${message(code: 'record.label', default: 'Record')}"/>
    <title><g:message code="default.create.label" args="[entityName]"/></title>
    <meta name="layout" content="main">
    <r:require modules="bootstrap"/>
</head>

<body>

<!-- Breadcrumbs -->
<div class="row">
    <ol class="breadcrumb">
        <li><g:link controller="zone" action="index">Zones</g:link></li>
        <li><g:link controller="record" action="index" params="${['domain.id':recordInstance?.domain?.id]}">${recordInstance?.domain?.name}</g:link></li>
        <li class="active">New</li>
    </ol>
</div>

<!-- Header and Alerts -->
<div class="row">
    <g:if test="${flash.message}">
        <div class="alert alert-info" role="status">${flash.message}</div>
    </g:if>
    <!--<p><g:link controller="record" action="index" params="${['domain.id':recordInstance?.domain?.id]}"> << Back to records ...</g:link></p>-->
</div>

<!-- Validation Errors -->
<g:hasErrors bean="${recordInstance}">
    <div class="row">
        <ul class="alert alert-danger" role="alert">
            <g:eachError bean="${recordInstance}" var="error">
                <li <g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>><g:message error="${error}"/></li>
            </g:eachError>
        </ul>
    </div>
</g:hasErrors>


<!-- Form -->
<div class="row">
<!-- Create Form -->
    <g:form url="[resource:recordInstance, action:'save']" role="form" class="form-horizontal">
        <g:hiddenField name="version" value="${recordInstance?.version}"/>
        <g:hiddenField name="domain.id" value="${recordInstance?.domain?.id}"/>

        <g:render template="form" />

        <div class="form-group">
            <div class="col-xs-6 col-sm-4 col-md-4 col-lg-4"></div>
            <div class="col-xs-6 col-sm-4 col-md-4 col-lg-4 pull-left">
                <g:submitButton name="create" class="btn btn-primary" value="${message(code: 'default.button.create.label', default: 'Create')}" />
                <g:link controller="record" action="index" params="${['domain.id':recordInstance?.domain?.id]}"><button type="reset" class="btn">${message(code: 'default.button.cancel.label', default: 'Cancel')}</button></g:link>
            </div>
        </div>
    </g:form>

</div>

</body>
</html>
