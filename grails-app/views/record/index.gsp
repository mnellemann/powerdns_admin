<%@ page import="biz.nellemann.powerdns.Record" %>
<!DOCTYPE html>
<html>
<head>
    <g:set var="entityName" value="${message(code: 'record.label', default: 'Record')}"/>
    <title><g:message code="default.list.label" args="[entityName]"/></title>
    <meta name="layout" content="main">
    <r:require modules="bootstrap"/>
</head>

<body>

<!-- Breadcrumbs -->
<div class="row">
    <ol class="breadcrumb">
        <li><g:link controller="zone" action="index">Zones</g:link></li>
        <!--<li><g:link controller="record" action="index" params="${['domain.id':domainInstance?.id]}">${domainInstance?.name}</g:link></li>-->
        <li class="active">${domainInstance?.name}</li>
    </ol>
</div>

<!-- Alerts and Header -->
<div class="row">
    <g:if test="${flash.message}">
        <div class="alert alert-info" role="status">${flash.message}</div>
    </g:if>
</div>


<!-- Create new record -->
<div class="row">
    <div class="text-info">
        <p><span class="glyphicon glyphicon-plus-sign"></span> <g:link action="create" params="['domain.id': domainInstance?.id]">Create a new Record</g:link></p>
    </div>
</div>


<!-- Records -->
<div class="row">
    <div class="table-responsive" id="listItems">
        <g:render template="list" />
    </div>
    <div>
        <g:paginate total="${recordInstanceCount ?: 0}" params="['domain.id': domainInstance?.id]" class="pagination-sm" />
    </div>
</div>


<!-- Account Linking -->



</body>
</html>
