<%@ page import="biz.nellemann.powerdns.Record" %>

<!-- Record Name -->
<div class="form-group  ${hasErrors(bean: recordInstance, field: 'name', 'has-error')} required">
	<label for="name" class="col-xs-6 col-sm-4 col-md-4 col-lg-4 control-label">
		<g:message code="record.name.label" default="Name" />
		<span class="required-indicator">*</span>
	</label>
    <div class="col-xs-6 col-sm-4 col-md-4 col-lg-4">
        <g:textField class="form-control" name="name" pattern="${recordInstance.constraints.name.matches}" required="" disabled="${(actionName == 'edit') ? 'true' : 'false'}" value="${(actionName == 'edit') ? recordInstance?.name : recordInstance?.domain?.name }" autocorrect="off" autocapitalize="off" />
        <!--<p class="help-block visible-md visible-lg">The name of the record must be a FQDN (eg. www.test.com) not ending with a dot.</p>-->
    </div>
</div>

<!-- Record Type -->
<div class="form-group ${hasErrors(bean: recordInstance, field: 'type', 'has-error')} required">
	<label class="col-xs-6 col-sm-4 col-md-4 col-lg-4 control-label" for="type">
		<g:message code="record.type.label" default="Type" />
		<span class="required-indicator">*</span>
	</label>
    <div class="col-xs-6 col-sm-4 col-md-4 col-lg-4">
	    <g:select class="form-control" name="type" from="${recordInstance?.getTypes()}" required="" disabled="${(actionName == 'edit') ? 'true' : 'false'}" value="${recordInstance?.type}" valueMessagePrefix="record.type"/>
    </div>
</div>

<!-- Content -->
<div class="form-group ${hasErrors(bean: recordInstance, field: 'content', 'has-error')} required">
	<label class="col-xs-6 col-sm-4 col-md-4 col-lg-4 control-label" for="content">
		<g:message code="record.content.label" default="Content" />
		<span class="required-indicator">*</span>
	</label>
    <div class="col-xs-6 col-sm-4 col-md-4 col-lg-4">
	    <g:textField class="form-control" name="content" pattern="${recordInstance.constraints.content.matches}" required="" value="${recordInstance?.content}" autocorrect="off" autocapitalize="off" />
        <!-- <p class="help-block visible-md visible-lg">Depends on the record type, see <a href="http://doc.powerdns.com/html/types.html">PowerDNS Types</a> for more information.</p>-->
    </div>
</div>

<!-- TTL -->
<div class="form-group ${hasErrors(bean: recordInstance, field: 'ttl', 'has-error')} ">
	<label class="col-xs-6 col-sm-4 col-md-4 col-lg-4 control-label" for="ttl">
		<g:message code="record.ttl.label" default="TTL" />
	</label>
    <div class="col-xs-6 col-sm-4 col-md-4 col-lg-4">
	    <g:field class="form-control" name="ttl" type="number" value="${recordInstance?.ttl}" autocorrect="off" autocapitalize="off" />
        <p class="help-block visible-md visible-lg">Time To Live (in seconds) for this record. This value is supplied in query responses to inform other servers how long they should keep the data in cache.</p>
    </div>
</div>

<!-- Priority -->
<div class="form-group ${hasErrors(bean: recordInstance, field: 'prio', 'has-error')} ">
	<label class="col-sm-4 col-md-4 col-lg-4 col-xs-6 control-label" for="prio">
		<g:message code="record.prio.label" default="Priority" />
	</label>
    <div class="col-xs-6 col-sm-4 col-md-4 col-lg-4">
	    <g:field class="form-control" name="prio" type="number" min="0" max="99" value="${recordInstance?.prio}" autocorrect="off" autocapitalize="off" />
        <p class="help-block visible-md visible-lg">Priority for eg. MX records.</p>
    </div>
</div>
