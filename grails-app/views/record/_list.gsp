<table class="table table-striped table-responsive table-hover">
    <thead>
    <tr>
        <g:sortableColumn property="name" title="${message(code: 'record.name.label', default: 'Record')}" params="['zone.id': domainInstance?.id]" />
        <g:sortableColumn property="type" title="${message(code: 'record.type.label', default: 'Type')}" params="['zone.id': domainInstance?.id]" />
        <g:sortableColumn property="content" title="${message(code: 'record.content.label', default: 'Points To')}" params="['zone.id': domainInstance?.id]" />
        <g:sortableColumn property="prio" title="${message(code: 'record.prio.label', default: 'Prio.')}" params="['zone.id': domainInstance?.id]"/>
        <g:sortableColumn property="ttl" title="${message(code: 'record.ttl.label', default: 'TTL')}" params="['zone.id': domainInstance?.id]" />
        <th>${message(code: 'record.action.label', default: 'Actions')}</th>
    </tr>
    </thead>
    <tbody>
    <g:each in="${recordInstanceList}" status="i" var="recordInstance">
        <tr class="${(i % 2) == 0 ? 'even' : 'odd'}" id="index_table${i}">

            <td><g:link action="edit" id="${recordInstance?.id}" params="zone.id: ${domainInstance?.id}">${fieldValue(bean: recordInstance, field: "name")}</g:link></td>
            <td>${fieldValue(bean: recordInstance, field: "type")}</td>
            <td><g:subString text="${recordInstance?.content}" max="20"/></td>
            <td>${fieldValue(bean: recordInstance, field: "prio")}</td>
            <td><g:formatNumber number="${recordInstance.ttl}" format="#######"/></td>
            <td>
                <tooltip:tip value="Edit record">
                    <g:link action="edit" id="${recordInstance?.id}" params="domain.id: ${domainInstance?.id}"><span class="glyphicon glyphicon-edit"></span></g:link>
                </tooltip:tip>
                <tooltip:tip value="Delete record">
                    <g:remoteLink
                        action="deleteItem"
                        id="${recordInstance?.id}"
                        update="listItems"
                        before="if(!confirm('Are you sure?')) return false">
                    <span class="glyphicon glyphicon-trash"></span></g:remoteLink>
                </tooltip:tip>
            </td>
        </tr>
    </g:each>
    </tbody>
</table>