
<%@ page import="biz.nellemann.powerdns.Record" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'record.label', default: 'Record')}" />
		<title><g:message code="default.show.label" args="[entityName]" /></title>
	</head>
	<body>
		<a href="#show-record" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div class="nav" role="navigation">
			<ul>
				<li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
				<li><g:link class="list" action="index"><g:message code="default.list.label" args="[entityName]" /></g:link></li>
				<li><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></li>
			</ul>
		</div>
		<div id="show-record" class="content scaffold-show" role="main">
			<h1><g:message code="default.show.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>
			<ol class="property-list record">
			
				<g:if test="${recordInstance?.name}">
				<li class="fieldcontain">
					<span id="name-label" class="property-label"><g:message code="record.name.label" default="Name" /></span>
					
						<span class="property-value" aria-labelledby="name-label"><g:fieldValue bean="${recordInstance}" field="name"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${recordInstance?.type}">
				<li class="fieldcontain">
					<span id="type-label" class="property-label"><g:message code="record.type.label" default="Type" /></span>
					
						<span class="property-value" aria-labelledby="type-label"><g:fieldValue bean="${recordInstance}" field="type"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${recordInstance?.content}">
				<li class="fieldcontain">
					<span id="content-label" class="property-label"><g:message code="record.content.label" default="Content" /></span>
					
						<span class="property-value" aria-labelledby="content-label"><g:fieldValue bean="${recordInstance}" field="content"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${recordInstance?.ttl}">
				<li class="fieldcontain">
					<span id="ttl-label" class="property-label"><g:message code="record.ttl.label" default="Ttl" /></span>
					
						<span class="property-value" aria-labelledby="ttl-label"><g:fieldValue bean="${recordInstance}" field="ttl"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${recordInstance?.prio}">
				<li class="fieldcontain">
					<span id="prio-label" class="property-label"><g:message code="record.prio.label" default="Prio" /></span>
					
						<span class="property-value" aria-labelledby="prio-label"><g:fieldValue bean="${recordInstance}" field="prio"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${recordInstance?.change_date}">
				<li class="fieldcontain">
					<span id="change_date-label" class="property-label"><g:message code="record.change_date.label" default="Changedate" /></span>
					
						<span class="property-value" aria-labelledby="change_date-label"><g:fieldValue bean="${recordInstance}" field="change_date"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${recordInstance?.zone}">
				<li class="fieldcontain">
					<span id="zone-label" class="property-label"><g:message code="record.zone.label" default="Zone" /></span>
					
						<span class="property-value" aria-labelledby="zone-label"><g:link controller="zone" action="show" id="${recordInstance?.zone?.id}">${recordInstance?.zone?.encodeAsHTML()}</g:link></span>
					
				</li>
				</g:if>
			
			</ol>
			<g:form url="[resource:recordInstance, action:'delete']" method="DELETE">
				<fieldset class="buttons">
					<g:link class="edit" action="edit" resource="${recordInstance}"><g:message code="default.button.edit.label" default="Edit" /></g:link>
					<g:actionSubmit class="delete" action="delete" value="${message(code: 'default.button.delete.label', default: 'Delete')}" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" />
				</fieldset>
			</g:form>
		</div>
	</body>
</html>
