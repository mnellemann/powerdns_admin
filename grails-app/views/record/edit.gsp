<%@ page import="biz.nellemann.powerdns.Record" %>
<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main">
    <r:require modules="bootstrap"/>
    <g:set var="entityName" value="${message(code: 'record.label', default: 'Record')}"/>
    <title><g:message code="default.edit.label" args="[entityName]"/></title>
</head>

<body>

<!-- Breadcrumbs -->
<div class="row">
    <ol class="breadcrumb">
        <li><g:link controller="zone" action="index">Zones</g:link></li>
        <li><g:link controller="record" action="index" params="${['domain.id':recordInstance?.domain?.id]}">${recordInstance?.domain?.name}</g:link></li>
        <li class="active">${recordInstance?.domain}</li>
    </ol>
</div>


<!-- Alerts -->
<div class="row">
    <g:if test="${flash.message}">
        <div class="alert alert-info" role="status">${flash.message}</div>
    </g:if>
</div>


<!-- Validation Errors -->
<g:hasErrors bean="${recordInstance}">
    <div class="row">
        <ul class="alert alert-danger" role="alert">
            <g:eachError bean="${recordInstance}" var="error">
                <li <g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>><g:message error="${error}"/></li>
            </g:eachError>
        </ul>
    </div>
</g:hasErrors>


<!-- Edit/Update Form -->
<div class="row">
    <g:form id="updateForm" class="form-horizontal" url="[resource:recordInstance, action:'update']" method="PUT" role="form">
        <g:hiddenField name="version" value="${recordInstance?.version}"/>
        <g:hiddenField name="domain.id" value="${recordInstance?.domain?.id}"/>
        <g:render template="form" />
    </g:form>

    <div class="form-group">
        <div class="col-xs-6 col-sm-4 col-md-4 col-lg-4"></div>
        <div class="col-xs-6 col-sm-4 col-md-4 col-lg-4 pull-left">
            <g:actionSubmit name="update" class="btn btn-primary" value="${message(code: 'default.button.update.label', default: 'Update')}" onclick="document.getElementById('updateForm').submit();" />
            <g:actionSubmit name="delete" class="btn btn-danger delete" value="${message(code: 'default.button.delete.label', default: 'Delete')}" data-toggle="modal" data-target="#deleteModal" />
            <g:link controller="record" action="index" params="['domain.id': recordInstance?.domain?.id]"><button type="reset" class="btn">${message(code: 'default.button.cancel.label', default: 'Cancel')}</button></g:link>
        </div>
    </div>
</div>


<!-- Dialogs -->
<div id="deleteModal" class="modal fade" role="dialog" tabindex="-1">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Delete Record</h4>
            </div>
            <div class="modal-body">
                <p>This record will be permanently deleted and cannot be recovered. Are you sure?</p>
                <g:form id="deleteForm" url="[resource:recordInstance, action:'delete']" method="DELETE">
                </g:form>
            </div>


            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                <button type="button" class="btn btn-danger" data-dismiss="modal" onclick="document.getElementById('deleteForm').submit();">Delete</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

</body>
</html>
