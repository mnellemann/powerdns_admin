<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta name="description" content="" />
    <meta name="author" content="" />
    <title>PowerDNS Admin - <g:layoutTitle/></title>
    <r:require modules="bootstrap,jquery"/>
    <link rel="shortcut icon" href="${resource(dir: 'images', file: 'favicon.ico')}" />
    <link rel="apple-touch-icon-precomposed" sizes="57x57" href="${resource(dir: 'images', file: 'apple-touch-icon.png')}" />
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="${resource(dir: 'images', file: 'apple-touch-icon-retina.png')}" />
    <r:layoutResources/>
    <tooltip:resources/>
</head>
<body style="padding-top: 60px; padding-bottom: 15px; padding-left: 5px; padding-right: 5px;">

<!-- Navigation -->
<div class="navbar navbar-default navbar-fixed-top" role="navigation" data-role="navigation">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="${createLink(uri: '/')}">PowerDNS Admin</a>
        </div>
        <div class="navbar-collapse collapse">
            <ul class="nav navbar-nav">

                <!-- Home -->
                <li><a href="${createLink(uri: '/home')}"><span class="glyphicon glyphicon-home"></span> Home</a></li>

                <!-- Zones -->
                <sec:ifLoggedIn>
                    <li><g:link controller="zone" action="index"><span class="glyphicon glyphicon-list"></span> Zones</g:link></li>
                </sec:ifLoggedIn>

                <!-- Templates -->
                <sec:ifLoggedIn>
                    <!--<li><g:link controller="template" action="index"><span class="glyphicon glyphicon-list-alt"></span> Templates</g:link></li>-->
                </sec:ifLoggedIn>

                <!-- SuperMasters -->
                <sec:ifAllGranted roles="ROLE_ADMIN">
                    <li><g:link controller="superMaster" action="index"><span class="glyphicon glyphicon-flash"></span> SuperMasters</g:link></li>
                </sec:ifAllGranted>

                <!-- Users -->
                <sec:ifAllGranted roles="ROLE_ADMIN">
                <li><g:link controller="user" action="search"><span class="glyphicon glyphicon-user"></span> Accounts</g:link></li>
                </sec:ifAllGranted>

            </ul>

            <ul class="nav navbar-nav navbar-right">
                <sec:ifNotLoggedIn>
                    <li><g:link controller='login' action='auth'><span class="glyphicon glyphicon-info-sign"></span> Login</g:link></li></sec:ifNotLoggedIn>
                <sec:ifLoggedIn>
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown"><span class="glyphicon glyphicon-info-sign"></span> <sec:username/><b class="caret"></b></a>
                        <ul class="dropdown-menu">
                            <li><g:link controller="profile" action="edit">Profile</g:link></li>
                            <li><a href="#">Settings</a></li>
                            <li><g:link controller='logout'>Sign out</g:link></li>
                        </ul>
                    </li>
                </sec:ifLoggedIn>
            </ul>
        </div><!--/.nav-collapse -->
    </div>
</div>

<!-- Begin page content -->
<div class="container">

    <!-- Content -->
    <g:layoutBody/>

</div> <!-- /container -->


<r:layoutResources/>

</body>
</html>