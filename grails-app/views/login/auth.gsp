<html>

<head>
<title><g:message code='spring.security.ui.login.title'/></title>
<meta name='layout' content='main'/>
</head>

<body>

<div class="row">

    <!--<p class="page-header"><g:message code="spring.security.ui.login.title"/></p>-->

    <g:if test='${flash.message}'>
        <div class="row">
            <div class='alert alert-warning'>${flash.message}</div>
        </div>
    </g:if>

    <div class="panel-heading">
        <legend><g:message code="springSecurity.login.header"/></legend>
    </div>


    <div class="panel-body">

        <form action='${postUrl}' method='POST' id='loginForm' role="form" autocomplete='off' class="form-horizontal">

            <div class="form-group">
                <label class="col-xs-6 col-sm-4 col-md-4 col-lg-4 control-label" for='username'><g:message code="springSecurity.login.username.label"/>:</label>
                <div class="col-xs-6 col-sm-4 col-md-4 col-lg-4">
                    <input type='text' class='form-control' name='j_username' id='username' autocorrect="off" autocapitalize="off"/>
                </div>
            </div>

            <div class="form-group">
                <label class="col-xs-6 col-sm-4 col-md-4 col-lg-4 control-label" for='password'><g:message code="springSecurity.login.password.label"/>:</label>
                <div class="col-xs-6 col-sm-4 col-md-4 col-lg-4">
                    <input class="form-control" type='password' name='j_password' id='password'/>
                </div>
            </div>

            <div class="form-group">
                <div class="col-xs-6 col-sm-4 col-md-4 col-lg-4 control-label">
                    <input class="btn btn-primary" type='submit' id="submit" value='${message(code: "springSecurity.login.button")}'/>
                </div>
                <div class="col-xs-6 col-sm-4 col-md-4 col-lg-4 checkbox-inline">
                    <input type="checkbox" class="checkbox" name="${rememberMeParameter}" id="remember_me" checked="checked" />
                    <label for='remember_me'><g:message code='spring.security.ui.login.rememberme'/></label>
                </div>
            </div>

            <div class="form-group">
                <span class="col-xs-12">
                    <g:link controller='register' action='forgotPassword'><g:message code='spring.security.ui.login.forgotPassword'/></g:link>
                </span><span class="col-xs-12">
                    <g:if test="${grailsApplication.config.grails.powerdns.register}">
                        <s2ui:linkButton elementId='register' controller='register' messageCode='spring.security.ui.login.register'/>
                    </g:if>
                </span>
            </div>


        </form>
    </div>

</div>

</body>
</html>
