<html>

<head>
<title><g:message code='spring.security.ui.forgotPassword.title'/></title>
<meta name='layout' content='main'/>
</head>

<body>


<div class="row">

    <g:if test='${flash.message}'>
        <div class="row">
            <div class='alert alert-warning'>${flash.message}</div>
        </div>
    </g:if>

    <div class="panel-heading">
        <legend><g:message code="spring.security.ui.forgotPassword.header"/></legend>
    </div>

    <div class="panel-body">

        <!-- Validation Errors -->
        <g:hasErrors bean="${command}">
            <div class="col-xs-12">
                <ul class="alert alert-danger" role="alert">
                    <g:eachError bean="${command}" var="error">
                        <li <g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>><g:message error="${error}"/></li>
                    </g:eachError>
                </ul>
            </div>
        </g:hasErrors>

        <g:form action='forgotPassword' name="forgotPasswordForm" autocomplete='off' role="form" class="form-horizontal">

            <g:if test='${emailSent}'>
                <div class="form-group col-xs-12">
                    <p class="text-success"><g:message code='spring.security.ui.forgotPassword.sent'/></p>
                </div>
            </g:if>
            <g:else>

                <div class="form-group col-xs-12">
                    <p class="text-info"><g:message code="spring.security.ui.forgotPassword.description"/></p>
                </div>

                <div class="form-group ${hasErrors(bean: command, field: 'username', 'has-error')} required">
                    <label class="col-xs-6 col-sm-4 col-md-4 col-lg-4 control-label" for='username'><g:message code="spring.security.ui.forgotPassword.username"/>:</label>
                    <div class="col-xs-6 col-sm-4 col-md-4 col-lg-4">
                        <input type='text' class='form-control' name='username' id='username' autocorrect="off" autocapitalize="off"/>
                    </div>
                </div>

                <div class="form-group ${hasErrors(bean: command, field: 'password', 'has-error')} required">
                    <div class="col-xs-6 col-sm-4 col-md-4 col-lg-4 control-label">
                        <input class="btn btn-primary" type='submit' id="submit" value='${message(code: "spring.security.ui.forgotPassword.submit")}'/>
                    </div>
                </div>

        	</g:else>

        </g:form>

    </div>

</body>
</html>
