<html>

<head>
	<meta name='layout' content='main'/>
	<title><g:message code='spring.security.ui.register.title'/></title>
</head>

<body>


<div class="row">

    <g:if test='${flash.message}'>
        <div class="row">
            <div class='alert alert-warning'>${flash.message}</div>
        </div>
    </g:if>

    <div class="panel-heading">
        <legend><g:message code="spring.security.ui.register.description"/></legend>
    </div>

    <div class="panel-body">

        <!-- Validation Errors -->
        <g:hasErrors bean="${command}">
            <div class="col-xs-12">
                <ul class="alert alert-danger" role="alert">
                    <g:eachError bean="${command}" var="error">
                        <li <g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>><g:message error="${error}"/></li>
                    </g:eachError>
                </ul>
            </div>
        </g:hasErrors>

        <g:form action='register' name='registerForm' role="form" autocomplete='off' class="form-horizontal">

            <g:if test='${emailSent}'>
                <div class="col-xs-12">
                    <p class="text-success"><g:message code='spring.security.ui.register.sent'/></p>
                </div>
            </g:if>
	        <g:else>

                <div class="form-group ${hasErrors(bean: command, field: 'username', 'has-error')} required">
                    <label class="col-xs-6 col-sm-4 col-md-4 col-lg-4 control-label" for='username'><g:message code="user.username.label" default="Username" />:</label>
                    <div class="col-xs-6 col-sm-4 col-md-4 col-lg-4">
                        <input type='text' class='form-control' name='username' id='username' value="${command.username}" autocorrect="off" autocapitalize="off"/>
                    </div>
                </div>

                <div class="form-group ${hasErrors(bean: command, field: 'email', 'has-error')} required">
                    <label class="col-xs-6 col-sm-4 col-md-4 col-lg-4 control-label" for='email'><g:message code="user.email.label" default="Email" />:</label>
                    <div class="col-xs-6 col-sm-4 col-md-4 col-lg-4">
                        <input type='text' class='form-control' name='email' id='email' value="${command.email}" autocorrect="off" autocapitalize="off"/>
                    </div>
                </div>

                <div class="form-group ${hasErrors(bean: command, field: 'password', 'has-error')} required">
                    <label class="col-xs-6 col-sm-4 col-md-4 col-lg-4 control-label" for='password'><g:message code="user.password.label" default="Password" />:</label>
                    <div class="col-xs-6 col-sm-4 col-md-4 col-lg-4">
                        <input type='password' class='form-control' name='password' id='password' value="${command.password}" autocorrect="off" autocapitalize="off"/>
                    </div>
                </div>

                <div class="form-group ${hasErrors(bean: command, field: 'password2', 'has-error')} required">
                    <label class="col-xs-6 col-sm-4 col-md-4 col-lg-4 control-label" for='password2'><g:message code="user.password2.label" default="Retype Password" />:</label>
                    <div class="col-xs-6 col-sm-4 col-md-4 col-lg-4">
                        <input type='password' class='form-control' name='password2' id='password2' value="${command.password2}" autocorrect="off" autocapitalize="off"/>
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-xs-6 col-sm-4 col-md-4 col-lg-4 control-label">
                        <input class="btn btn-primary" type='submit' id="submit" value='${message(code: "spring.security.ui.register.submit")}'/>
                    </div>
                </div>
            </g:else>

        </g:form>

    </div>

</div>

</body>
</html>
