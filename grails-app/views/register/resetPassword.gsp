<html>

<head>
<title><g:message code='spring.security.ui.resetPassword.title'/></title>
<meta name='layout' content='main'/>
</head>

<body>

<div class="row">

    <g:if test='${flash.message}'>
        <div class="row">
            <div class='alert alert-warning'>${flash.message}</div>
        </div>
    </g:if>

    <div class="panel-heading">
        <legend><g:message code="spring.security.ui.resetPassword.header"/></legend>
    </div>

    <div class="panel-body">

        <!-- Validation Errors -->
        <g:hasErrors bean="${command}">
            <div class="col-xs-12">
                <ul class="alert alert-danger" role="alert">
                    <g:eachError bean="${command}" var="error">
                        <li <g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>><g:message error="${error}"/></li>
                    </g:eachError>
                </ul>
            </div>
        </g:hasErrors>

        <g:form action='resetPassword' name='resetPasswordForm' autocomplete='off' role="form" class="form-horizontal">
            <g:hiddenField name='t' value='${token}'/>

            <div class="form-group col-xs-12">
                <p class="text-info"><g:message code="spring.security.ui.resetPassword.description"/></p>
            </div>

            <div class="form-group ${hasErrors(bean: command, field: 'password', 'has-error')} required">
                <label class="col-xs-6 col-sm-4 col-md-4 col-lg-4 control-label" for='password'><g:message code="spring.security.ui.resetPassword.password.label" default="Password" />:</label>
                <div class="col-xs-6 col-sm-4 col-md-4 col-lg-4">
                    <input type='password' class='form-control' name='password' id='password' autocorrect="off" autocapitalize="off"/>
                </div>
            </div>

            <div class="form-group ${hasErrors(bean: command, field: 'password2', 'has-error')} required">
                <label class="col-xs-6 col-sm-4 col-md-4 col-lg-4 control-label" for='password2'><g:message code="spring.security.ui.resetPassword.password2.label" default="Retype Password" />:</label>
                <div class="col-xs-6 col-sm-4 col-md-4 col-lg-4">
                    <input type='password' class='form-control' name='password2' id='password2' autocorrect="off" autocapitalize="off"/>
                </div>
            </div>

            <div class="form-group">
                <div class="col-xs-6 col-sm-4 col-md-4 col-lg-4 control-label">
                    <input class="btn btn-primary" type='submit' id="submit" value='${message(code: "spring.security.ui.resetPassword.submit")}'/>
                </div>
            </div>

        </div>
        </g:form>

    </div>

</body>
</html>
