<%@ page import="biz.nellemann.powerdns.Zone" %>
<!DOCTYPE html>
<html>
<head>
    <g:set var="entityName" value="${message(code: 'zone.label', default: 'Zone')}"/>
    <title><g:message code="default.list.label" args="[entityName]"/></title>
    <meta name="layout" content="main">
    <r:require modules="bootstrap"/>
</head>

<body>

<!-- Breadcrumbs -->
<div class="row">
    <ol class="breadcrumb">
        <li class="active">Zones</li>
    </ol>
</div>

<!-- Alerts and header -->
<div class="row">
    <g:if test="${flash.message}">
        <div class="alert alert-info" role="status">${flash.message}</div>
    </g:if>
</div>

<!-- Create new zone -->
<div class="row">
    <div class="text-info">
        <p><span class="glyphicon glyphicon-plus-sign"></span> <g:link controller="zone" action="create" params="['user.id': sec.loggedInUserInfo(field: 'id').toInteger()]">Create a new Zone</g:link></p>
    </div>
</div>


<!-- List of zones -->
<div class="row">
    <div class="table-responsive" id="listItems">
        <g:render template="list" />
    </div>
    <div>
        <g:paginate total="${zoneInstanceCount ?: 0}" class="pagination-sm" />
    </div>
</div>

</body>
</html>
