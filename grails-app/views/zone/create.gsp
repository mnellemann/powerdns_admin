<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main">
    <r:require modules="bootstrap"/>
    <g:set var="entityName" value="${message(code: 'zone.label', default: 'Zone')}"/>
    <title><g:message code="default.create.label" args="[entityName]"/></title>
</head>

<body>

<!-- Breadcrumbs -->
<div class="row">
    <ol class="breadcrumb">
        <li><g:link controller="zone" action="index">Zones</g:link></li>
        <li class="active">New</li>
    </ol>
</div>

<!-- Header and Alerts -->
<div class="row">
    <g:if test="${flash.message}">
        <div class="alert alert-info" role="status">${flash.message}</div>
    </g:if>
    <!--<p><g:link controller="zone" action="index"> << Back to zones ...</g:link></p>-->
</div>

<!-- Validation Errors -->
<g:hasErrors bean="${zoneInstance}">
    <div class="row">
        <ul class="alert alert-danger" role="alert">
            <g:eachError bean="${zoneInstance}" var="error">
                <li <g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>><g:message error="${error}"/></li>
            </g:eachError>
        </ul>
    </div>
</g:hasErrors>

<!-- Create Zone -->
<div class="row">
    <g:form class="form-horizontal" url="[resource:zoneInstance, action:'save']" role="form">
        <g:hiddenField name="version" value="${zoneInstance?.version}"/>
        <g:hiddenField name="user.id" value="${sec.loggedInUserInfo(field: 'id')}"/>
        <g:render template="form" />

        <div class="form-group">
            <div class="col-xs-6 col-sm-4 col-md-4 col-lg-4"></div>
            <div class="col-xs-6 col-sm-4 col-md-4 col-lg-4 pull-left">
                <g:submitButton name="create" class="btn btn-primary" value="${message(code: 'default.button.create.label', default: 'Create')}" />
                <g:link controller="zone" action="index"><button type="reset" class="btn">${message(code: 'default.button.cancel.label', default: 'Cancel')}</button></g:link>
            </div>
        </div>
    </g:form>
</div>

</body>
</html>