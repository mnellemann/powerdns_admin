
<%@ page import="biz.nellemann.powerdns.Zone" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'zone.label', default: 'Zone')}" />
		<title><g:message code="default.show.label" args="[entityName]" /></title>
	</head>
	<body>
		<a href="#show-zone" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div class="nav" role="navigation">
			<ul>
				<li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
				<li><g:link class="list" action="index"><g:message code="default.list.label" args="[entityName]" /></g:link></li>
				<li><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></li>
			</ul>
		</div>
		<div id="show-zone" class="content scaffold-show" role="main">
			<h1><g:message code="default.show.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>
			<ol class="property-list zone">
			
				<g:if test="${zoneInstance?.name}">
				<li class="fieldcontain">
					<span id="name-label" class="property-label"><g:message code="zone.name.label" default="Name" /></span>
					
						<span class="property-value" aria-labelledby="name-label"><g:fieldValue bean="${zoneInstance}" field="name"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${zoneInstance?.master}">
				<li class="fieldcontain">
					<span id="master-label" class="property-label"><g:message code="zone.master.label" default="Master" /></span>
					
						<span class="property-value" aria-labelledby="master-label"><g:fieldValue bean="${zoneInstance}" field="master"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${zoneInstance?.last_check}">
				<li class="fieldcontain">
					<span id="last_check-label" class="property-label"><g:message code="zone.last_check.label" default="Lastcheck" /></span>
					
						<span class="property-value" aria-labelledby="last_check-label"><g:fieldValue bean="${zoneInstance}" field="last_check"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${zoneInstance?.type}">
				<li class="fieldcontain">
					<span id="type-label" class="property-label"><g:message code="zone.type.label" default="Type" /></span>
					
						<span class="property-value" aria-labelledby="type-label"><g:fieldValue bean="${zoneInstance}" field="type"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${zoneInstance?.notified_serial}">
				<li class="fieldcontain">
					<span id="notified_serial-label" class="property-label"><g:message code="zone.notified_serial.label" default="Notifiedserial" /></span>
					
						<span class="property-value" aria-labelledby="notified_serial-label"><g:fieldValue bean="${zoneInstance}" field="notified_serial"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${zoneInstance?.account}">
				<li class="fieldcontain">
					<span id="account-label" class="property-label"><g:message code="zone.account.label" default="Account" /></span>
					
						<span class="property-value" aria-labelledby="account-label"><g:fieldValue bean="${zoneInstance}" field="account"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${zoneInstance?.records}">
				<li class="fieldcontain">
					<span id="records-label" class="property-label"><g:message code="zone.records.label" default="Records" /></span>
					
						<g:each in="${zoneInstance.records}" var="r">
						<span class="property-value" aria-labelledby="records-label"><g:link controller="record" action="show" id="${r.id}">${r?.encodeAsHTML()}</g:link></span>
						</g:each>
					
				</li>
				</g:if>
			
			</ol>
			<g:form url="[resource:zoneInstance, action:'delete']" method="DELETE">
				<fieldset class="buttons">
					<g:link class="edit" action="edit" resource="${zoneInstance}"><g:message code="default.button.edit.label" default="Edit" /></g:link>
					<g:actionSubmit class="delete" action="delete" value="${message(code: 'default.button.delete.label', default: 'Delete')}" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" />
				</fieldset>
			</g:form>
		</div>
	</body>
</html>
