<%@ page import="biz.nellemann.powerdns.Zone" %>

<!-- Zone Name -->
<div class="form-group ${hasErrors(bean: zoneInstance, field: 'name', 'has-error')} required">
    <label class="col-xs-6 col-sm-4 col-md-4 col-lg-4 control-label" for="name">
        <g:message code="zone.name.label" default="Zone Name"/>
        <span class="required-indicator">*</span>
    </label>
    <div class="col-xs-6 col-sm-4 col-md-4 col-lg-4">
        <g:textField class="form-control" name="name" pattern="${zoneInstance.constraints.name.matches}" required="" disabled="${(actionName == 'edit') ? 'true' : 'false'}" value="${zoneInstance?.name}" />
        <p class="help-block visible-md visible-lg">The root name of this zone, eg. 'test.com', not ending with a dot.</p>
    </div>
</div>

<!-- Zone Type -->
<div class="form-group ${hasErrors(bean: zoneInstance, field: 'type', 'has-error')} required">
    <label for="type" class="col-xs-6 col-sm-4 col-md-4 col-lg-4 control-label">
        <g:message code="zone.type.label" default="Zone Type"/>
        <span class="required-indicator">*</span>
    </label>
    <div class="col-xs-6 col-sm-4 col-md-4 col-lg-4">
        <g:select class="form-control" name="type" from="${zoneInstance.constraints.type.inList}" required="" disabled="${(actionName == 'edit') ? 'true' : 'false'}" value="${zoneInstance?.type}" valueMessagePrefix="zone.type" />
        <p class="help-block visible-md visible-lg">The PowerDNS zone type - currently only NATIVE and MASTER are supported.</p>
    </div>
</div>


<!-- Primary Nameserver -->
<div class="form-group ${hasErrors(bean: zoneInstance, field: 'soa_primary', 'has-error')} required">
    <label class="col-xs-6 col-sm-4 col-md-4 col-lg-4 control-label" for="soa_primary">
        <g:message code="zone.primary.label" default="Primary NS"/>
        <span class="required-indicator">*</span>
    </label>
    <div class="col-xs-6 col-sm-4 col-md-4 col-lg-4">
        <g:textField class="form-control" name="soa_primary" pattern="${zoneInstance.constraints.soa_primary.matches}" required="" disabled="${(actionName == 'edit') ? 'true' : 'false'}" value="${zoneInstance?.soa_primary}" autocorrect="off" autocapitalize="off" />
        <p class="help-block visible-md visible-lg">The primary and authorative nameserver of this zone.</p>
    </div>
</div>

<!-- Hostmaster -->
<div class="form-group ${hasErrors(bean: zoneInstance, field: 'soa_hostmaster', 'has-error')} required">
    <label class="col-xs-6 col-sm-4 col-md-4 col-lg-4 control-label" for="soa_hostmaster">
        <g:message code="zone.hostmaster.label" default="Hostmaster"/>
        <span class="required-indicator">*</span>
    </label>
    <div class="col-xs-6 col-sm-4 col-md-4 col-lg-4">
        <g:textField class="form-control" name="soa_hostmaster" pattern="${zoneInstance.constraints.soa_hostmaster.matches}" required="" value="${zoneInstance?.soa_hostmaster}" type="email" autocorrect="off" autocapitalize="off" />
        <p class="help-block visible-md visible-lg">Hostmaster email, eg. hostmaster@your-zone.com.</p>
    </div>
</div>

<!-- Serial -->
<div class="form-group ${hasErrors(bean: zoneInstance, field: 'soa_serial', 'has-error')} required">
    <label class="col-xs-6 col-sm-4 col-md-4 col-lg-4 control-label" for="soa_serial">
        <g:message code="zone.serial.label" default="Serial"/>
        <span class="required-indicator">*</span>
    </label>
    <div class="col-xs-6 col-sm-4 col-md-4 col-lg-4">
        <g:textField class="form-control" name="soa_serial" required="" value="${zoneInstance?.soa_serial}" autocorrect="off" autocapitalize="off" />
        <p class="help-block visible-md visible-lg">If left to 0 (the default) the highest change-date field of the records within the zone will be used.</p>
    </div>
</div>

<!-- Refresh -->
<div class="form-group ${hasErrors(bean: zoneInstance, field: 'soa_refresh', 'has-error')} required">
    <label class="col-xs-6 col-sm-4 col-md-4 col-lg-4 control-label" for="soa_refresh">
        <g:message code="zone.refresh.label" default="Refresh Time"/>
        <span class="required-indicator">*</span>
    </label>
    <div class="col-xs-6 col-sm-4 col-md-4 col-lg-4">
        <g:textField class="form-control" name="soa_refresh" required="" value="${zoneInstance?.soa_refresh}" autocorrect="off" autocapitalize="off" />
        <p class="help-block visible-md visible-lg">The time, in seconds, a secondary DNS server waits before querying the primary DNS server's SOA record to check for changes.</p>
    </div>
</div>

<!-- Retry -->
<div class="form-group ${hasErrors(bean: zoneInstance, field: 'soa_retry', 'has-error')} required">
    <label class="col-xs-6 col-sm-4 col-md-4 col-lg-4 control-label" for="soa_retry">
        <g:message code="zone.retry.label" default="Retry Time"/>
        <span class="required-indicator">*</span>
    </label>
    <div class="col-xs-6 col-sm-4 col-md-4 col-lg-4">
        <g:textField class="form-control" name="soa_retry" required="" value="${zoneInstance?.soa_retry}" autocorrect="off" autocapitalize="off" />
        <p class="help-block visible-md visible-lg">The time, in seconds, a secondary server waits before retrying a failed zone transfer. Normally, the retry time is less than the refresh time.</p>
    </div>
</div>

<!-- Expire -->
<div class="form-group ${hasErrors(bean: zoneInstance, field: 'soa_expire', 'has-error')} required">
    <label class="col-xs-6 col-sm-4 col-md-4 col-lg-4 control-label" for="soa_expire">
        <g:message code="zone.expire.label" default="Expire Time"/>
        <span class="required-indicator">*</span>
    </label>
    <div class="col-xs-6 col-sm-4 col-md-4 col-lg-4">
        <g:textField class="form-control" name="soa_expire" required="" value="${zoneInstance?.soa_expire}" autocorrect="off" autocapitalize="off" />
        <p class="help-block visible-md visible-lg">The time, in seconds, that a secondary server will keep trying to complete a zone transfer. If this time expires prior to a successful zone transfer, the secondary server will expire its zone file.</p>
    </div>
</div>

<!-- Min. TTL -->
<div class="form-group ${hasErrors(bean: zoneInstance, field: 'soa_ttl', 'has-error')} required">
    <label class="col-xs-6 col-sm-4 col-md-4 col-lg-4 control-label" for="soa_ttl">
        <g:message code="zone.ttl.label" default="Minimum TTL"/>
        <span class="required-indicator">*</span>
    </label>
    <div class="col-xs-6 col-sm-4 col-md-4 col-lg-4">
        <g:textField class="form-control" name="soa_ttl" required="" value="${zoneInstance?.soa_ttl}" autocorrect="off" autocapitalize="off" />
        <p class="help-block visible-md visible-lg">The minimum time-to-live value applies to all resource records in the zone file. This value is supplied in query responses to inform other servers how long they should keep the data in cache.</p>
    </div>
</div>

<!-- Reverse Zone -->
<div class="form-group  ${hasErrors(bean: zoneInstance, field: 'reverse', 'has-error')}">
    <label class="col-xs-6 col-sm-4 col-md-4 col-lg-4 control-label" for="reverse">
        <g:message code="zone.reverse.label" default="Reverse Zone"/>
    </label>
    <div class="col-xs-6 col-sm-4 col-md-4 col-lg-4">
        <g:checkBox class="checkbox" name="reverse" value="${zoneInstance?.reverse}" disabled="${(actionName == 'edit') ? 'true' : 'false'}"/>
        <p class="help-block visible-md visible-lg">A reverse zone is used to map hostnames back to ip-addresses.</p>

    </div>
</div>
