<table class="table table-striped table-responsive table-hover">
    <thead>
    <tr>
        <g:sortableColumn property="name" title="${message(code: 'zone.name.label', default: 'Name')}"/>
        <g:sortableColumn property="soa_primary" title="${message(code: 'zone.primary.label', default: 'Primary')}"/>
        <g:sortableColumn property="soa_hostmaster" title="${message(code: 'zone.hostmaster.label', default: 'Hostmaster')}"/>
        <g:sortableColumn property="type" title="${message(code: 'zone.type.label', default: 'Type')}"/>
        <th>${message(code: 'zone.action.label', default: 'Actions')}</th>
    </tr>
    </thead>
    <tbody>
    <g:each in="${zoneInstanceList}" status="i" var="zoneInstance">
        <tr class="${(i % 2) == 0 ? 'even' : 'odd'}">
            <td><g:link controller="record" params="['domain.id': zoneInstance?.id]">${fieldValue(bean: zoneInstance, field: "name")}</g:link></td>
            <td>${fieldValue(bean: zoneInstance, field: "soa_primary")}</td>
            <td>${fieldValue(bean: zoneInstance, field: "soa_hostmaster")}</td>
            <td>${fieldValue(bean: zoneInstance, field: "type")}</td>
            <td>
                <tooltip:tip value="Records">
                    <g:link controller="record" params="['domain.id': zoneInstance?.id]">
                        <span class="glyphicon glyphicon-list"></span>
                   </g:link>
                </tooltip:tip>
                <tooltip:tip value="Edit zone">
                    <g:link action="edit" id="${zoneInstance?.id}"><span class="glyphicon glyphicon-edit"></span></g:link>
                </tooltip:tip>
                <g:if test="${zoneInstance.active}">
                    <tooltip:tip value="Suspend zone">
                        <g:remoteLink
                                action="suspendItem"
                                id="${zoneInstance?.id}"
                                update="listItems" >
                        <span class="glyphicon glyphicon-pause"></span>
                        </g:remoteLink>
                    </tooltip:tip>
                </g:if>
                <g:else>
                    <tooltip:tip value="Release zone">
                        <g:remoteLink
                                action="releaseItem"
                                id="${zoneInstance?.id}"
                                update="listItems" >
                            <span class="glyphicon glyphicon-play"></span>
                        </g:remoteLink>
                    </tooltip:tip>
                </g:else>
            </td>
        </tr>
    </g:each>
    </tbody>
</table>