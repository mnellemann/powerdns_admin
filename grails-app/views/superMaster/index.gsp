<%@ page import="biz.nellemann.powerdns.SuperMaster" %>
<!DOCTYPE html>
<html>
<head>
    <g:set var="entityName" value="${message(code: 'superMaster.label', default: 'SuperMaster')}"/>
    <title><g:message code="default.list.label" args="[entityName]"/></title>
    <meta name="layout" content="main">
    <r:require modules="bootstrap"/>
</head>

<body>

<!-- Breadcrumbs -->
<div class="row">
    <ol class="breadcrumb">
        <li class="active">SuperMasters</li>
    </ol>
</div>

<!-- Alerts and header -->
<div class="row">
    <g:if test="${flash.message}">
        <div class="alert alert-info" role="status">${flash.message}</div>
    </g:if>
</div>

<!-- Create new domain -->
<div class="row">
    <div class="text-info">
        <p><span class="glyphicon glyphicon-plus-sign"></span> <g:link action="create">Create a new SuperMaster</g:link></p>
    </div>
</div>


<!-- List of domains -->
<div class="row">
    <div class="table-responsive" id="listItems">
        <g:render template="list" />
    </div>
    <div>
        <g:paginate total="${smInstanceCount ?: 0}" class="pagination-sm" />
    </div>
</div>

<div class="row">
    <div class="well well-lg">
        Before a SuperMaster notification succeeds, the following conditions must be met:
        <ul>
            <li>The supermaster must carry a SOA record for the notified domain</li>
            <li>The supermaster IP must be present in our list above</li>
            <li>The set of NS records for the domain, as retrieved by the slave from the supermaster,
            must include the hostname that goes with the IP address listed above.</li>
        </ul>
    </div>
    <div class="well well-lg">
        To enable the use of PowerDNS SuperMasters, you must also enable slave operations (slave=yes) in your pdns.conf configuration file.
    </div>
</div>


</body>
</html>
