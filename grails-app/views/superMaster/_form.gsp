<%@ page import="biz.nellemann.powerdns.SuperMaster" %>

<!-- IP Address -->
<div class="form-group ${hasErrors(bean: superMasterInstance, field: 'ip', 'has-error')} required">
    <label class="col-xs-6 col-sm-4 col-md-4 col-lg-4 control-label" for="ip">
        <g:message code="supermaster.ip.label" default="IP Address"/>
        <span class="required-indicator">*</span>
    </label>
    <div class="col-xs-6 col-sm-4 col-md-4 col-lg-4">
        <g:textField class="form-control" name="ip" required="" value="${superMasterInstance?.ip}" />
        <p class="help-block visible-md visible-lg">The IP address of the nameserver notifying us.</p>
    </div>
</div>

<!-- Nameserver -->
<div class="form-group ${hasErrors(bean: superMasterInstance, field: 'nameserver', 'has-error')} required">
    <label for="name" class="col-xs-6 col-sm-4 col-md-4 col-lg-4 control-label">
        <g:message code="supermaster.nameserver.label" default="Nameserver"/>
        <span class="required-indicator">*</span>
    </label>
    <div class="col-xs-6 col-sm-4 col-md-4 col-lg-4">
        <g:textField class="form-control" name="nameserver" required="" value="${superMasterInstance?.nameserver}" />
        <p class="help-block visible-md visible-lg">Must be a valid NS record from the zones we are being slave of.</p>
    </div>
</div>
