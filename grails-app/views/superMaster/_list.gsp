<table class="table table-striped table-responsive table-hover">
    <thead>
    <tr>
        <g:sortableColumn property="ip" title="${message(code: 'supermaster.ip.label', default: 'IP Address')}"/>
        <g:sortableColumn property="nameserver" title="${message(code: 'supermaster.nameserver.label', default: 'Nameserver')}"/>
        <th>${message(code: 'domain.action.label', default: 'Actions')}</th>
    </tr>
    </thead>
    <tbody>
    <g:each in="${superMasterInstanceList}" status="i" var="superMasterInstance">
        <tr class="${(i % 2) == 0 ? 'even' : 'odd'}">
            <td>${fieldValue(bean: superMasterInstance, field: "ip")}</td>
            <td>${fieldValue(bean: superMasterInstance, field: "nameserver")}</td>
            <td>
                <tooltip:tip value="Edit domain">
                    <g:link action="edit" id="${superMasterInstance?.id}"><span class="glyphicon glyphicon-edit"></span></g:link>
                </tooltip:tip>
            </td>
        </tr>
    </g:each>
    </tbody>
</table>