<!-- Current Password -->
<div class="form-group ${hasErrors(bean: userInstance, field: 'password', 'has-error')} required">
    <label class="col-xs-6 col-sm-4 col-md-4 col-lg-4 control-label" for="password">
        <g:message code="user.password-current.label" default="Current Password"/>
    </label>
    <div class="col-xs-6 col-sm-4 col-md-4 col-lg-4">
        <g:passwordField class="form-control" name="password" value="" autocorrect="off" autocapitalize="off" />
        <p class="help-block visible-md visible-lg">Enter your current password.</p>
    </div>
</div>


<!-- New Password -->
<div class="form-group ${hasErrors(bean: userInstance, field: 'password_new', 'has-error')} required">
    <label class="col-xs-6 col-sm-4 col-md-4 col-lg-4 control-label" for="password_new">
        <g:message code="user.password-new.label" default="Current Password"/>
    </label>
    <div class="col-xs-6 col-sm-4 col-md-4 col-lg-4">
        <g:passwordField class="form-control" name="password_new" value="" autocorrect="off" autocapitalize="off" />
        <p class="help-block visible-md visible-lg">Enter your new password.</p>
    </div>
</div>

<!-- New Password2 -->
<div class="form-group ${hasErrors(bean: userInstance, field: 'password_new_2', 'has-error')} required">
    <label class="col-xs-6 col-sm-4 col-md-4 col-lg-4 control-label" for="password_new_2">
        <g:message code="user.password-retype.label" default="Re-type Password"/>
    </label>
    <div class="col-xs-6 col-sm-4 col-md-4 col-lg-4">
        <g:passwordField class="form-control" name="password_new_2" value="" autocorrect="off" autocapitalize="off" />
        <p class="help-block visible-md visible-lg">Re-type your new password.</p>
    </div>
</div>