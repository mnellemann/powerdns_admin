<%@ page import="biz.nellemann.powerdns.User" %>


<!-- First Name -->
<div class="form-group ${hasErrors(bean: userInstance, field: 'firstName', 'has-error')} required">
    <label class="col-xs-6 col-sm-4 col-md-4 col-lg-4 control-label" for="firstName">
        <g:message code="user.firstname.label" default="First Name"/>
    </label>
    <div class="col-xs-6 col-sm-4 col-md-4 col-lg-4">
        <g:textField class="form-control" name="firstName" value="${userInstance?.firstName}" autocorrect="on" autocapitalize="on" />
        <p class="help-block visible-md visible-lg">First name.</p>
    </div>
</div>


<!-- Last Name -->
<div class="form-group ${hasErrors(bean: userInstance, field: 'lastName', 'has-error')} required">
    <label class="col-xs-6 col-sm-4 col-md-4 col-lg-4 control-label" for="lastName">
        <g:message code="user.lastname.label" default="Last Name"/>
    </label>
    <div class="col-xs-6 col-sm-4 col-md-4 col-lg-4">
        <g:textField class="form-control" name="lastName" value="${userInstance?.lastName}" autocorrect="on" autocapitalize="on" />
        <p class="help-block visible-md visible-lg">Last name.</p>
    </div>
</div>


<!-- Email -->
<div class="form-group ${hasErrors(bean: userInstance, field: 'email', 'has-error')} required">
    <label class="col-xs-6 col-sm-4 col-md-4 col-lg-4 control-label" for="email">
        <g:message code="user.email.label" default="Email"/>
        <span class="required-indicator">*</span>
    </label>
    <div class="col-xs-6 col-sm-4 col-md-4 col-lg-4">
        <g:textField class="form-control" name="email" pattern="${userInstance.constraints.email.matches}" required="" value="${userInstance?.email}" autocorrect="on" autocapitalize="on" />
        <p class="help-block visible-md visible-lg">Valid Email </p>
    </div>
</div>
