<%@ page import="biz.nellemann.powerdns.User" %>
<!DOCTYPE html>
<html>
<head>
    <g:set var="entityName" value="${message(code: 'profile.label', default: 'Profile')}"/>
    <title><g:message code="default.list.label" args="[entityName]"/></title>
    <meta name="layout" content="main">
    <r:require modules="bootstrap"/>
</head>


<body>

<!-- Header and Alerts -->
<div class="row">
    <g:if test="${flash.message}">
        <div class="alert alert-info" role="status">${flash.message}</div>
    </g:if>
    <g:if test="${flash.error}">
        <div class="alert alert-danger" role="status">${flash.error}</div>
    </g:if>
</div>


<!-- Validation Errors -->
<g:hasErrors bean="${userInstance}">
    <div class="row">
        <ul class="alert alert-danger" role="alert">
            <g:eachError bean="${userInstance}" var="error">
                <li <g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>><g:message error="${error}"/></li>
            </g:eachError>
        </ul>
    </div>
</g:hasErrors>


<!-- Edit/Update Form -->
<div class="row">

    <p class="text-info">
        Change password for ${userInstance?.username}
    </p>

    <g:form id="updateForm" class="form-horizontal" url="[controller:'profile', action:'updatePassword']" method="PUT" role="form">
        <g:hiddenField name="version" value="${userInstance?.version}"/>
        <g:hiddenField name="id" value="${userInstance?.id}"/>
        <g:render template="password" />
    </g:form>

    <div class="form-group">
        <div class="col-xs-6 col-sm-4 col-md-4 col-lg-4"></div>
        <div class="col-xs-6 col-sm-4 col-md-4 col-lg-4 pull-left">
            <g:actionSubmit name="update" class="btn btn-primary" value="${message(code: 'default.button.update.label', default: 'Update')}" onclick="document.getElementById('updateForm').submit();" />
        </div>
    </div>


</div>




</body>
</html>