<!DOCTYPE html>
<html>
<head>
    <title>Home</title>
    <meta name="layout" content="main"/>
</head>
<body>

<div class="jumbotron">
    <h1>Welcome </h1>
    <p>... to PowerDNS Admin!</p>
</div>

<div class="row">

    <div class="col-xs-12">
        <div class="well well-lg">
            <p >
                <em>PowerDNS Admin</em> is a web administration interface for managing DNS domains and records.
            </p>
            <p>Please visit the <a href="http://mark.nellemann.nu/powerdns-admin">project website</a> for more information.</p>
        </div>
    </div>

    <div class="col-xs-12">
        <br/><br/>
        <p class="text-muted">This is <em>PowerDNS Admin</em> version v.<g:meta name="app.version"/>.</p>
    </div>


 </div>

</body>
</html>