/*
    Copyright (C) 2014  Mark Nellemann <mark.nellemann@gmail.com>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

// locations to search for config files that get merged into the main config;
// config files can be ConfigSlurper scripts, Java properties files, or classes
// in the classpath in ConfigSlurper format

grails.config.locations = ["classpath:${appName}-config.groovy",
                       "file:${userHome}/.grails/${appName}-config.groovy",
                       "file:./${appName}-config.groovy"]
if (System.properties["${appName}.config.location"]) {
   grails.config.locations << "file:" + System.properties["${appName}.config.location"]
}

grails.project.groupId = appName // change this to alter the default package name and Maven publishing destination

// The ACCEPT header will not be used for content negotiation for user agents containing the following strings (defaults to the 4 major rendering engines)
grails.mime.disable.accept.header.userAgents = ['Gecko', 'WebKit', 'Presto', 'Trident']
grails.mime.types = [
        all:           '*/*',
        atom:          'application/atom+xml',
        css:           'text/css',
        csv:           'text/csv',
        form:          'application/x-www-form-urlencoded',
        html:          ['text/html','application/xhtml+xml'],
        js:            'text/javascript',
        json:          ['application/json', 'text/json'],
        multipartForm: 'multipart/form-data',
        rss:           'application/rss+xml',
        text:          'text/plain',
        hal:           ['application/hal+json','application/hal+xml'],
        xml:           ['text/xml', 'application/xml']
]

// URL Mapping Cache Max Size, defaults to 5000
//grails.urlmapping.cache.maxsize = 1000

// What URL patterns should be processed by the resources plugin
grails.resources.adhoc.patterns = ['/images/*', '/css/*', '/js/*', '/plugins/*']

// Legacy setting for codec used to encode data with ${}
grails.views.default.codec = "html"

// The default scope for controllers. May be prototype, session or singleton.
// If unspecified, controllers are prototype scoped.
grails.controllers.defaultScope = 'singleton'


// GSP settings
grails {
    views {
        gsp {
            encoding = 'UTF-8'
            htmlcodec = 'xml' // use xml escaping instead of HTML4 escaping
            codecs {
                expression = 'html' // escapes values inside ${}
                scriptlet = 'html' // escapes output from scriptlets in GSPs
                taglib = 'none' // escapes output from taglibs
                staticparts = 'none' // escapes output from static template parts
            }
        }
        // escapes all not-encoded output at final stage of outputting
        filteringCodecForContentType {
            //'text/html' = 'html'
        }
    }

    mail {
        host = "vps18.comin.dk"
        port = 25
        username = "powerdns@nellemann.biz"
        password = "vaij0xie9Iet"
        props = ["mail.smtp.auth":"true"]
        /*props = ["mail.smtp.auth":"true",
                "mail.smtp.socketFactory.port":"465",
                "mail.smtp.socketFactory.class":"javax.net.ssl.SSLSocketFactory",
                "mail.smtp.socketFactory.fallback":"false"]*/
    }

    powerdns {
        // Enable new account registration
        register = true
    }
}

grails.converters.encoding = "UTF-8"
// scaffolding templates configuration
grails.scaffolding.templates.domainSuffix = 'Instance'

// Set to false to use the new Grails 1.2 JSONBuilder in the render method
grails.json.legacy.builder = false
// enabled native2ascii conversion of i18n properties files
grails.enable.native2ascii = true
// packages to include in Spring bean scanning
grails.spring.bean.packages = []
// whether to disable processing of multi part requests
grails.web.disable.multipart=false

// request parameters to mask when logging exceptions
grails.exceptionresolver.params.exclude = ['password']

// configure auto-caching of queries by default (if false you can cache individual queries with 'cache: true')
grails.hibernate.cache.queries = false

environments {
    development {
        grails.logging.jul.usebridge = true
    }
    production {
        grails.logging.jul.usebridge = false
        // TODO: grails.serverURL = "http://www.changeme.com"
        grails.mail.host = 'localhost'
        grails.mail.props = ["mail.smtp.auth":"false"]

    }
}

// log4j configuration
log4j = {
    // Example of changing the log pattern for the default console appender:
    //
    //appenders {
    //    console name:'stdout', layout:pattern(conversionPattern: '%c{2} %m%n')
    //}

    error  'org.codehaus.groovy.grails.web.servlet',        // controllers
            'org.codehaus.groovy.grails.web.pages',          // GSP
            'org.codehaus.groovy.grails.web.sitemesh',       // layouts
            'org.codehaus.groovy.grails.web.mapping.filter', // URL mapping
            'org.codehaus.groovy.grails.web.mapping',        // URL mapping
            'org.codehaus.groovy.grails.commons',            // core / classloading
            'org.codehaus.groovy.grails.plugins',            // plugins
            'org.codehaus.groovy.grails.orm.hibernate',      // hibernate integration
            'org.springframework',
            'org.hibernate',
            'net.sf.ehcache.hibernate'
}


// Added by the Spring Security Core plugin:
grails.plugin.springsecurity.userLookup.userDomainClassName = 'biz.nellemann.powerdns.User'
grails.plugin.springsecurity.userLookup.authorityJoinClassName = 'biz.nellemann.powerdns.UserRole'
grails.plugin.springsecurity.authority.className = 'biz.nellemann.powerdns.Role'


// Allow basic auth for /api/ urls
grails.plugin.springsecurity.useBasicAuth = true
grails.plugin.springsecurity.basic.realmName = "PowerDNS Admin"
grails.plugin.springsecurity.filterChain.chainMap = [
        '/api/**': 'JOINED_FILTERS,-exceptionTranslationFilter',
        '/**': 'JOINED_FILTERS,-basicAuthenticationFilter,-basicExceptionTranslationFilter'
]

// Setup security levels for different urls
grails.plugin.springsecurity.securityConfigType = "InterceptUrlMap"
grails.plugin.springsecurity.interceptUrlMap = [
        '/':                  ['permitAll'],
        '/index':             ['permitAll'],
        '/index.gsp':         ['permitAll'],
        '/error':             ['permitAll'],
        '/dbconsole':         ['permitAll'],
        '/dbconsole/**':      ['permitAll'],
        '/home':              ['permitAll'],
        '/**/js/**':          ['permitAll'],
        '/**/css/**':         ['permitAll'],
        '/**/images/**':      ['permitAll'],
        '/**/favicon.ico':    ['permitAll'],
        '/home':              ['ROLE_USER', 'isAuthenticated()'],
        '/zone/**':           ['ROLE_USER', 'isAuthenticated()'],
        '/record/**':         ['ROLE_USER', 'isAuthenticated()'],
        '/template/**':       ['ROLE_USER', 'isAuthenticated()'],
        '/superMaster/**':    ['ROLE_ADMIN', 'isAuthenticated()'],
        '/login/**':          ['permitAll'],
        '/logout/**':         ['permitAll'],
        '/register/**':       ['permitAll'],
        '/profile/**':        ['ROLE_USER', 'isFullyAuthenticated()'],
        '/user/**':           ['ROLE_ADMIN', 'isFullyAuthenticated()'],
        '/role/**':           ['ROLE_ADMIN', 'isFullyAuthenticated()'],
        '/securityInfo/**':   ['ROLE_ADMIN', 'isFullyAuthenticated()'],
        '/persistentLogin/**': ['ROLE_ADMIN', 'isFullyAuthenticated()'],
        '/registrationCode/**': ['ROLE_ADMIN', 'isFullyAuthenticated()'],
        '/j_spring_security_switch_user': ['ROLE_ADMIN'],
        '/j_spring_security_exit_user':   ['permitAll'],
        '/api/**':            ['ROLE_ADMIN', 'isFullyAuthenticated()']
]

grails.plugin.springsecurity.rememberMe.persistent = true
grails.plugin.springsecurity.rememberMe.persistentToken.domainClassName = 'biz.nellemann.powerdns.PersistentLogin'
grails.plugin.springsecurity.logout.postOnly = false // Allow GET request to logout


/* Spring Security UI */
//grails.plugin.springsecurity.ui.register.defaultRoleNames = ['ROLE_CUSTOMER']
//grails.plugin.springsecurity.ui.register.postRegisterUrl = '/welcome'
//grails.plugin.springsecurity.ui.forgotPassword.postResetUrl


// jQuery
grails.views.javascript.library="jquery"

// Twitter Bootstrap plugin
grails.plugins.twitterbootstrap.fixtaglib = true
grails.plugins.twitterbootstrap.defaultBundle = 'bundle_bootstrap'
