class UrlMappings {

	static mappings = {

        /*
        "/record/index/$domainId?(.${format})?"{
            controller = 'record'
            action = 'index'
        }*/



        //"/api/v1/zones"(resource: 'zone', namespace: 'v1')
        //"/api/v1/zones"(resources: 'zone')

        "/$controller/$action?/$id?(.$format)?"{
            constraints {
                // apply constraints here
            }
        }

        "/"(view:"/index")
        "500"(view:'/error')
        "/home"(view:'/home')

	}
}
