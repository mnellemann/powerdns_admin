/*
    Copyright (C) 2014  Mark Nellemann <mark.nellemann@gmail.com>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

import biz.nellemann.powerdns.Role
import biz.nellemann.powerdns.User
import biz.nellemann.powerdns.UserRole
import biz.nellemann.powerdns.Zone
import biz.nellemann.powerdns.Record
import biz.nellemann.powerdns.SuperMaster

class BootStrap {

    def init = { servletContext ->

        // Bootstrap administrative roles and users
        if(! Role.findByAuthority('ROLE_SUPERUSER')) {
            def superRole = new Role(authority: 'ROLE_SUPERUSER').save(failOnError: true)
            if(superRole.hasErrors()) {
                println superRole.errors
            }
        }
        if(! Role.findByAuthority('ROLE_ADMIN')) {
            def adminRole = new Role(authority: 'ROLE_ADMIN').save(failOnError: true)
            if(adminRole.hasErrors()) {
                println adminRole.errors
            }
        }
        if(! Role.findByAuthority('ROLE_USER')) {
            def userRole = new Role(authority: 'ROLE_USER').save(failOnError: true)
            if(userRole.hasErrors()) {
                println userRole.errors
            }
        }
        if(! Role.findByAuthority('ROLE_SWITCH_USER')) {
            def switchRole = new Role(authority: 'ROLE_SWITCH_USER').save(failOnError: true)
            if(switchRole.hasErrors()) {
                println switchRole.errors
            }
        }

        // Create Administrator
        if(! User.findByUsername('hostmaster')) {
            def adminUser = new User(username: 'hostmaster', password: 'secret', email: 'hostmaster@nellemann.biz', firstName: 'Hostmaster', lastName: 'Doe').save(failOnError: true)
            if(adminUser.hasErrors()) {
                println adminUser.errors
            }

            // Assign adminUser to adminRole and userRole

            def superRole = Role.findByAuthority('ROLE_SUPERUSER')
            UserRole.create adminUser, superRole, true

            def adminRole = Role.findByAuthority('ROLE_ADMIN')
            UserRole.create adminUser, adminRole, true

            def userRole = Role.findByAuthority('ROLE_USER')
            UserRole.create adminUser, userRole, true

            def switchRole = Role.findByAuthority('ROLE_SWITCH_USER')
            UserRole.create adminUser, switchRole, true

        }




        // Test data for development
        if(grails.util.Environment.isDevelopmentMode()) {

            // Lookup hostmaster
            def user = User.findByUsername('hostmaster')

            // Create a reverse zone
            if(! Zone.findByName('0.168.192.in-addr.arpa')) {
                def revZone = new Zone(name: '0.168.192.in-addr.arpa', reverse: true, soa_primary: 'vps14.comin.dk', soa_hostmaster: 'mark@comin.dk', user: user).save(failOnError: true)
                revZone.addToRecords(new Record(name: '1.0.168.192.in-addr.arpa', type: 'PTR', content: 'some.ptr.host.com'))

                //user.addToDomains(revZone)
            }

            // Forward zones
            if(! Zone.findByName('nellemann.biz')) {
                def myZone = new Zone(name: 'nellemann.biz', soa_primary: 'ns.comin.dk', soa_hostmaster: 'hostmaster@comin.dk', user: user).save(failOnError: true)

                // Add the zone to the admin user
                //user.addToDomains(myZone)
                myZone.addToRecords(new Record(name: 'nellemann.biz', type: 'NS', content: 'ns1.nellemann.biz'))
                myZone.addToRecords(new Record(name: 'nellemann.biz', type: 'NS', content: 'ns2.nellemann.biz'))
                myZone.addToRecords(new Record(name: 'nellemann.biz', type: 'NS', content: 'ns3.nellemann.biz'))
                myZone.addToRecords(new Record(name: 'nellemann.biz', type: 'NS', content: 'ns4.nellemann.biz'))
                myZone.addToRecords(new Record(name: 'nellemann.biz', type: 'MX', prio: 5, content: 'vps18.comin.dk'))
                myZone.addToRecords(new Record(name: 'nellemann.biz', type: 'MX', prio: 10, content: 'vps14.comin.dk'))
                myZone.addToRecords(new Record(name: 'ns1.nellemann.biz',  type: 'A', content: '213.150.52.2'))
                myZone.addToRecords(new Record(name: 'ns2.nellemann.biz',  type: 'A', content: '213.150.52.3'))
                myZone.addToRecords(new Record(name: 'ns3.nellemann.biz',  type: 'A', content: '213.150.52.4'))
                myZone.addToRecords(new Record(name: 'ns4.nellemann.biz',  type: 'A', content: '213.150.52.5'))
                myZone.addToRecords(new Record(name: 'www.nellemann.biz', type: 'CNAME', content: 'vps16.comin.dk'))
                myZone.addToRecords(new Record(name: 'webmail.nellemann.biz', type: 'CNAME', content: 'mail.nellemann.biz'))
                myZone.addToRecords(new Record(name: 'ftp.nellemann.biz', type: 'CNAME', content: 'vps16.comin.dk'))
            }

            // SuperMaster
            if(! SuperMaster.findByIp('213.150.52.2')) {
                def mySuperMaster = new SuperMaster(ip: '213.150.52.2', nameserver: 'ns.comin.dk').save(failOnError: true)
            }


                def i = 1
                while(i < 25) {
                    def myZone = new Zone(name: "test${i}.com", soa_primary: 'ns.comin.dk', soa_hostmaster: 'hostmaster@comin.dk', user: user).save(failOnError: true)

                    // Add the zone to the admin user
                    //user.addToDomains(myZone)

                    myZone.addToRecords(new Record(name: "test${i}.com", type: 'NS', content: "ns1.test${i}.com"))
                    myZone.addToRecords(new Record(name: "test${i}.com", type: 'NS', content: "ns2.test${i}.com"))
                    myZone.addToRecords(new Record(name: "test${i}.com", type: 'MX', prio: 5, content: 'vps18.comin.dk'))
                    myZone.addToRecords(new Record(name: "test${i}.com", type: 'MX', prio: 10, content: 'vps14.comin.dk'))
                    myZone.addToRecords(new Record(name: "ns1.test${i}.com",  type: 'A', content: '213.150.52.2'))
                    myZone.addToRecords(new Record(name: "ns2.test${i}.com",  type: 'A', content: '213.150.52.3'))
                    myZone.addToRecords(new Record(name: "ns3.test${i}.com",  type: 'A', content: '213.150.52.4'))
                    myZone.addToRecords(new Record(name: "ns4.test${i}.com",  type: 'A', content: '213.150.52.5'))
                    myZone.addToRecords(new Record(name: "webmail.test${i}.com", type: 'CNAME', content: "mail.test${i}.com"))
                    myZone.addToRecords(new Record(name: "ftp.test${i}.com", type: 'CNAME', content: 'vps16.comin.dk'))

                    i++
                }


                // Create a demo User
                if(! User.findByUsername('demo')) {
                    def demoUser = new User(username: 'demo', password: 'demo', email: 'demo@your-domain.com', firstName: 'Demonstration', lastName: 'Account').save(failOnError: true)
                    if(demoUser.hasErrors()) {
                        println demoUser.errors
                    }

                    // Assign demoUser to userRole
                    def userRole = Role.findByAuthority('ROLE_USER')
                    UserRole.create demoUser, userRole, true
                }

                // Create a forward domain for the demo user
                if(! Zone.findByName('demo.com')) {
                    def demoUser = User.findByUsername('demo')
                    def demoZone = new Zone(name: 'demo.com', soa_primary: 'ns.comin.dk', soa_hostmaster: 'hostmaster@comin.dk', user: demoUser).save(failOnError: true)

                    // Add the zone to the admin user
                    //demoUser.addToDomains(demoZone)

                    demoZone.addToRecords(new Record(name: 'mail.demo.com',  type: 'A', content: '213.150.52.18'))
                    demoZone.addToRecords(new Record(name: 'demo.com', type: 'MX', prio: 5, content: 'mail.demo.com'))
                }

        }
    }

    def destroy = {

    }
}
