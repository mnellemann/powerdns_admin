/*
    Copyright (C) 2014  Mark Nellemann <mark.nellemann@gmail.com>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

package biz.nellemann.powerdns

class GeneralTagLib {
    static defaultEncodeAs = 'html'
    //static encodeAsForTags = [email: 'raw']

    def subString = { attrs, body ->
        if(attrs.text?.size() > (attrs.max as Integer) ) {
            out << org.apache.commons.lang.StringUtils.substring( attrs.text, 0, ((attrs.max as Integer)-4) ) + " ..."
        } else {
            out << attrs.text
        }
    }

    def email = { attrs, body ->

        def addr = attrs.addr

        //out << addr.replaceAll(/\./, ' . ').replaceAll('@', ' (at) ')
        out << addr.replaceAll('@', '(at)')
    }




}
