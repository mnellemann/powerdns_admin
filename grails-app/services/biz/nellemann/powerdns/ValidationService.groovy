/*
    Copyright (C) 2014  Mark Nellemann <mark.nellemann@gmail.com>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package biz.nellemann.powerdns

import org.codehaus.groovy.grails.validation.routines.InetAddressValidator
import org.codehaus.groovy.grails.validation.routines.DomainValidator

class ValidationService {

    /*
     Domain Names
    */

    // Match against a generic domain pattern. eg.  domain.com  or something.domain.com
    def static domainNamePattern = /^([0-9A-Za-z]{2,}\.[0-9A-Za-z]{2,3}\.[0-9A-Za-z]{2,3}|[0-9A-Za-z]{2,}\.[0-9A-Za-z]{2,4})$/

    // Match against a reverse zone name eg.  0.168.192.in-addr.arpa
    def static domainReverseNamePattern = ~/^([\d]+\.)+in-addr.arpa$/
    // TODO: what about IPv6 ?


    /*
     Record Names
    */

    def static recordNamePattern = ~/^[A-Za-z0-9-]+(\.[A-Za-z0-9-]+)*(\.[A-Za-z]{2,4})$/
    def static recordReverseNamePattern = ~/^([\d]+\.)+in-addr.arpa$/


    /*
     Record Content
    */

    def static ipv4Pattern = ~/^(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$/
    def static ipv6Pattern = ~/^([\dA-F]{1,4}:|((?=.*(::))(?!.*\3.+\3))\3?)([\dA-F]{1,4}(\3|:\b)|\2){5}(([\dA-F]{1,4}(\3|:\b|$)|\2){2}|(((2[0-4]|1\d|[1-9])?\d|25[0-5])\.?\b){4})\z/
    def static txtPattern = ~/^[A-Za-z0-9-=]+/       // See http://tools.ietf.org/html/rfc1464 for valid content



    // Validate the name of a domain
    static validateDomainName(String name, Boolean reverse) {

        def ret = true

        if(reverse) {
            if(!name.matches(domainReverseNamePattern)) {
                ret = 'domain.validation.error.reverse'
            }
        } else {
            if(!DomainValidator.getInstance().isValid(name)) {
                ret = 'domain.validation.error.domain'
            }
        }



        return ret

    }


    // Validate the name of a record
    static validateRecordName(String name, String type, String domain, Boolean reverse) {

        // Make sure it looks like a proper PTR name
        // TODO: is this still needed with the 'PTR' check below?
        if(reverse && !name.matches(recordReverseNamePattern)) {
            println(" >> validation error: reverse with name ${name}")
            return 'record.validation.error.reverse'
        }

        // Cases based on the record type
        switch(type) {

            // We test that the PTR record name end with the domain
            case 'PTR':
                // TODO: what about IPv6 ?
                if(!name.matches(/^([0-9]+)\.${domain}/)) {
                    println(" >> validation error: reverse name ${name} not in ${domain}")
                    return 'record.validation.error.reverse'
                }
                break;

            // TODO: support _sip._tcp .domain.com, this seems to work (([a-z0-9_]+)\.)?([a-z0-9_]+\.)test.com
            case 'SRV':
                if(!name.matches(/^(([a-z0-9_]+)\.)?([a-z0-9_]+\.)${domain}/)) {
                    println(" >> validation error: name ${name} not in ${domain}")
                    return 'record.validation.error.srv'
                }
                break;

            // For all other record types ...
            default:

                // Make a general test to see if this looks like a proper domain name
                if(!reverse && !DomainValidator.getInstance().isValid(name) ) {
                    println(" >> validation error: forward with name ${name}")
                    return 'record.validation.error.forward'
                }

                // We want to make sure that the record name ends with our own domain name
                if(!reverse && !name.matches(/^([A-Za-z0-9-]+\.)?${domain}/)) {
                    println(" >> validation error: name ${name} not in ${domain}")
                    return 'record.validation.error.name'
                }

                break

        }

        return true
    }


    // Validate the content of a record based on the record type
    static validateRecordContent(String content, String type, String name, String domain, Boolean reverse) {

        switch (type) {

            case 'SOA':
                // Users are not allowed to make this record, so we trust it's OK
                return true
                break

            case 'A':
                // Should be a valid IP address
                if(!InetAddressValidator.getInstance().isValidInet4Address(content)) {
                    return 'record.validation.error.ipv4'
                }
                break

            case 'AAAA':
                // TODO: Should be a valid IPv6 address
                return 'record.validation.error.ipv6'
                break

            case 'MX':
                // Should be a valid host/domain-name
                if(!DomainValidator.getInstance().isValid(content)) {
                    return 'record.validation.error.mx'
                }
                break

            case 'NS':
                // Should be a valid host/domain-name
                if(!DomainValidator.getInstance().isValid(content)) {
                    return 'record.validation.error.ns'
                }
                break

            case 'CNAME':
                // Should be a valid host/domain-name and not point to itself
                if(content == name) {
                    return 'record.validation.error.cname.self'
                } else if(name == domain) {
                    return 'record.validation.error.cname.domain'
                } else {
                    if(!DomainValidator.getInstance().isValid(content)) {
                        return 'record.validation.error.cname'
                    }
                }
                break

            case 'PTR':
                if(!DomainValidator.getInstance().isValid(content)) {
                    return 'record.validation.error.ptr'
                }
                break

            case 'TXT':
                if(!content.matches(txtPattern)) {
                    return 'record.validation.error.txt'
                }
                break

            case 'SRV':
                if(!content.matches(txtPattern)) {
                    return 'record.validation.error.srv'
                }
                break;

        }

        //println(" >> validation error: record of type ${type} cannot be ${content}")
        return true
    }


    // Validate the record type against our allowed types
    static validateRecordType(String type, List<String> types, Boolean reverse) {

        // Allow SOA and NS records always
        if(['SOA', 'NS'].contains(type)) {
            return true
        }

        // Reverse zones: only accept PTR records
        if(reverse && type == 'PTR') {
            return true
        }

        // Forward zones: accept listed types
        if( types.contains(type) ) {
            return true
        }

        println(" >> validation error: record type ${type} not valid");
        return 'record.validation.error.type'
    }


    static validateIpAddress(String val) {

        // Todo ipv6
        if(!InetAddressValidator.getInstance().isValidInet4Address(val)) {
            return 'record.validation.error.ipv4'
        }

        return true
    }

}
